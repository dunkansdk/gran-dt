package grandt.utils;

/**
 * <p>Constantes varias</p>
 */
public class Constants {

	// equipos (pa�ses)
	public static final int ID_EQUIPO_RUSIA = 0;
	public static final int ID_EQUIPO_ARABIA_SAUDITA = 1;
	public static final int ID_EQUIPO_EGIPTO = 2;
	public static final int ID_EQUIPO_URUGUAY = 3;
	public static final int ID_EQUIPO_IRAN = 4;
	public static final int ID_EQUIPO_MARRUECOS = 5;
	public static final int ID_EQUIPO_PORTUGAL = 6;
	public static final int ID_EQUIPO_ESPANA = 7;
	public static final int ID_EQUIPO_AUSTRALIA = 8;
	public static final int ID_EQUIPO_DINAMARCA = 9;
	public static final int ID_EQUIPO_FRANCIA = 10;
	public static final int ID_EQUIPO_PERU = 11;
	public static final int ID_EQUIPO_ARGENTINA = 12;
	public static final int ID_EQUIPO_CROACIA = 13;
	public static final int ID_EQUIPO_ISLANDIA = 14;
	public static final int ID_EQUIPO_NIGERIA = 15;
	public static final int ID_EQUIPO_BRASIL = 16;
	public static final int ID_EQUIPO_COSTA_RICA = 17;
	public static final int ID_EQUIPO_SERBIA = 18;
	public static final int ID_EQUIPO_SUIZA = 19;
	public static final int ID_EQUIPO_ALEMANIA = 20;
	public static final int ID_EQUIPO_COREA_SUR = 21;
	public static final int ID_EQUIPO_MEXICO = 22;
	public static final int ID_EQUIPO_SUECIA = 23;
	public static final int ID_EQUIPO_BELGICA = 24;
	public static final int ID_EQUIPO_INGLATERRA = 25;
	public static final int ID_EQUIPO_PANAMA = 26;
	public static final int ID_EQUIPO_TUNEZ = 27;
	public static final int ID_EQUIPO_COLOMBIA = 28;
	public static final int ID_EQUIPO_JAPON = 29;
	public static final int ID_EQUIPO_POLONIA = 30;
	public static final int ID_EQUIPO_SENEGAL = 31;
	
	// grupos
	public static final int ID_GRUPO_A = 0;
	public static final int ID_GRUPO_B = 1;
	public static final int ID_GRUPO_C = 2;
	public static final int ID_GRUPO_D = 3;
	public static final int ID_GRUPO_E = 4;
	public static final int ID_GRUPO_F = 5;
	public static final int ID_GRUPO_G = 6;
	public static final int ID_GRUPO_H = 7;
	
	// posiciones de campo
	public static final int POS_ARQUERO = 0;
	public static final int POS_DEFENSOR = 1;
	public static final int POS_VOLANTE = 2;
	public static final int POS_DELANTERO = 3;
	
	public static final String NOMBRE_EQUIPO_DEFAULT = "NombreDefault";
	
	// para calcular la cantidad de situaciones de gol de un determinado equipo en un partido
	public static final int SITUACION_GOL_MODIFIER = 950;
	
	// para estimar la cantidad de faltas de un determinado equipo en un partido
	public static final int FALTAS_MODIFIER = 11;
	
	// para calculo de puntajes
	public static final int PUNTAJE_DEFAULT = 50;
	public static final int PUNTOS_GOL = 20;
	public static final int PUNTOS_ASISTENCIA = 10;
	public static final int PUNTOS_AMARILLA = 5; // negativos
	public static final int PUNTOS_GOL_EN_CONTRA = 10; // negativos
}
