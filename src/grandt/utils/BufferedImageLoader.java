package grandt.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * <p>Carga los recursos gr�ficos (principalmente usados en el display de la cancha)</p>
 */
public class BufferedImageLoader {

	public static BufferedImage load(String path) //TODO: meter el throws aca?
	{
		BufferedImage img = null;
		
		try {
		    img = ImageIO.read(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return img;
	}
	
}
