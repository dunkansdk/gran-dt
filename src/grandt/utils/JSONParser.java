package grandt.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONObject;

/**
 * <p>Clase que se encarga de leer los archivos JSON y retornarlos como JSON object.</p>
 * @see <a href="https://github.com/stleary/JSON-java">Repositorio JSON.org</a>
 */
public class JSONParser {
	
	private static JSONObject jObject;

	public static JSONObject parse(String path)
	{
		String content;
			    
		try {
			content = readFile(StandardCharsets.UTF_8, path);
			
			// cnvert JSON string to JSONObject
		    jObject = new JSONObject(content);		    
		} catch (IOException e) {
			e.printStackTrace();
			
			jObject = null;
		}
		
		return jObject;
	}
	
	private static String readFile(Charset encoding, String path) throws IOException 
	{
	  byte[] encoded = Files.readAllBytes(Paths.get(path));
	  return new String(encoded, encoding);
	}

}
