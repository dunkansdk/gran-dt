package grandt.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

/**
 * <p>Utilidades varias.</p>
 */
public class CommonUtils {

	public static HashMap<String, Integer> sortByComparator(HashMap<String, Integer> unsortMap, final boolean order)
    {
 
        List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsortMap.entrySet());
 
        // Sorting the list based on values
        Collections.sort(list, new Comparator<Entry<String, Integer>>()
        {
            public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2)
            {
                if (order)
                {
                    return o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    return o2.getValue().compareTo(o1.getValue());
                }
            }
        });
 
        // Maintaining insertion order with the help of LinkedList
        HashMap<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Entry<String, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
 
        return sortedMap;
    }
	
	public static String getGrupo(int n)
	{
		switch(n)
		{
			case Constants.ID_GRUPO_A: return "A";
			case Constants.ID_GRUPO_B: return "B";
			case Constants.ID_GRUPO_C: return "C";
			case Constants.ID_GRUPO_D: return "D";
			case Constants.ID_GRUPO_E: return "E";
			case Constants.ID_GRUPO_F: return "F";
			case Constants.ID_GRUPO_G: return "G";
			case Constants.ID_GRUPO_H: return "H";
			default: return "A";
		}
	}
	
}
