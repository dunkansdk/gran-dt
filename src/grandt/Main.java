package grandt;

import grandt.base.DT;
import grandt.base.Torneo;
import grandt.base.estados.StateManager;
import grandt.base.loaders.DateLoader;
import grandt.base.loaders.TeamLoader;
import grandt.base.stats.Stats;

/**
 * <p>Clase Main. S�lo tiene el m�todo <i>main()</i>, que inicializa el torneo.</p>
 */
public class Main {
	
	public static void main(String[] args) {
		
		// inicializamos las clases est�ticas
		Torneo.init();
		DT.init();

		// cargamos los equipos
		Torneo.loadEquipos(TeamLoader.load("data/data.json"));
		
		// cargamos las fechas (de la fase de grupos; las fechas eliminatorias se general programaticamente)
		Torneo.addFecha(DateLoader.load("data/fechas/fecha1_grupos.json")); // Fecha 1
		Torneo.addFecha(DateLoader.load("data/fechas/fecha2_grupos.json")); // Fecha 2
		Torneo.addFecha(DateLoader.load("data/fechas/fecha3_grupos.json")); // Fecha 3
		
		// inicializamos la estadisticas
		Stats.init();
		
		// inicializamos las ventanitas
		StateManager.init();
	}
}
