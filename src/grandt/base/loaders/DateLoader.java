package grandt.base.loaders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.json.JSONObject;

import grandt.base.Partido;
import grandt.base.Torneo;
import grandt.base.equipos.EquipoDefault;
import grandt.base.fechas.Fecha;
import grandt.utils.JSONParser;

/**
 * <p>Clase encargada de cargar las fechas desde los JSON al Torneo.</p>
 * @see grandt.utils.JSONParser
 */
public class DateLoader {
		
	/**
	 * <p>Carga una fecha (cada fecha est� en un JSON separado) y la retorna</p>
	 * @param path 		&emsp;(<b>String</b>) la ruta del JSON de la fecha
	 * @return 			({@link grandt.base.fechas.Fecha Fecha}) la fecha cargada
	 */
	public static Fecha load(String path)
	{

		// guardamos los datos del JSON parseado en el objeto JSON "fechasObject"
		JSONObject fechasObject = JSONParser.parse(path);
		
		// cantidad de partidos que hay en la fecha (para loopear)
		int numPartidos = fechasObject.getInt("num_partidos");
		
		// arreglo de partidos para la Fecha
		ArrayList<Partido> partidos = new ArrayList<Partido>();
		
		// loopeamos los partidos de la fecha
		for(int i = 0; i < numPartidos; i++)
		{
			// guardamos los datos del partido en el objeto JSON "partidoActualObject"
			JSONObject partidoActualObject = fechasObject.getJSONObject(String.valueOf(i));
			
			// creamos dos equipos (local y visitante) y accedemos a los datos del Torneo para cada equipo
			EquipoDefault equipoLocal = Torneo.getEquipo(partidoActualObject.getString("team1"));
			EquipoDefault equipoVisitante = Torneo.getEquipo(partidoActualObject.getString("team2"));

			// guardamos la fecha (dia) del partido como un LocalDate, pero nos obligan a formatearlo (malditos yankees)
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			LocalDate fechaDia = LocalDate.parse(partidoActualObject.getString("dia"), dtf);
			
			// agregamos el nuevo partido al array de partidos de la fecha
			partidos.add(new Partido(equipoLocal, equipoVisitante, partidoActualObject.getString("grupo"), fechaDia));
		}
		
		// creamos la fecha a retornar
		Fecha f = new Fecha(partidos);
		
		// retornamos la fecha creada
		return f;
	}
}
