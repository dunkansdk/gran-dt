package grandt.base.loaders;

import java.util.ArrayList;

import org.json.JSONObject;

import grandt.base.equipos.EquipoDefault;
import grandt.base.exceptions.IncompleteTeamException;
import grandt.base.jugador.Arquero;
import grandt.base.jugador.Campo;
import grandt.base.jugador.Jugador;
import grandt.utils.Constants;
import grandt.utils.JSONParser;

/**
 * <p>Clase encargada de cargar los datos de todos los equipos desde el JSON al Torneo.</p>
 * @see grandt.utils.JSONParser
 */
public class TeamLoader {
		
	private static final int NUM_JUGADORES = 11;
		
	/**
	 * <p>Carga los equipos a un arreglo ylo retorna</p>
	 * @param path 							&emsp;(<b>String</b>) la ruta del JSON a cargar
	 * @return ({@link java.util.ArrayList ArrayList[EquipoDefault]}) el arreglo de equipos
	 * @see grandt.base.equipos.EquipoDefault EquipoDefault
	 */
	public static ArrayList<EquipoDefault> load(String path) 
	{
		ArrayList<EquipoDefault> equipos = new ArrayList<EquipoDefault>();
		ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
		
		// guardamos en "object" los datos del JSON parseado
		JSONObject object = JSONParser.parse(path);
		
		int size = object.getInt("num_equipos");
		
		// guardamos en "teams" el objeto "equipos" del JSON
		JSONObject teams = object.getJSONObject("equipos");
		
		// recorremos los equipos del JSON
		for(int i = 0; i < size; i++)
		{			
			jugadores = new ArrayList<Jugador>();
			
			// guardamos los datos el equipo actual en el objeto JSON "actual"
			JSONObject actual = teams.getJSONObject(String.valueOf(i));
			
			// variables del equipo actual
			String nombreEquipo = actual.getString("nombre");
			String currPath = actual.getString("path");
			String grupo = actual.getString("grupo");

			// cargamos los datos específicos del equipo accediendo al JSON correspondiente
			JSONObject equipoactual = JSONParser.parse("data/equipos/" + currPath);
			
			// guardamos los datos de los jugadores en el objeto JSON "jugadoresactual" (jugadores del equipo actual)
			JSONObject jugadoresactual = equipoactual.getJSONObject("jugadores");

			// loopeamos los jugadores
			for(int j = 0; j < NUM_JUGADORES; j++) {
				
				// variable donde vamos a guardar el jugador actual una vez cargados los datos
				Jugador jugador;
				
				// el jugador actual del loop se guarda en el objeto JSON "jugadoractual"
				JSONObject jugadoractual = jugadoresactual.getJSONObject(String.valueOf(j));
				
				// variables para los datos del jugador actual (tomadas del objeto JSON)
				String nombreJugador = jugadoractual.getString("nombre");
				int pos = jugadoractual.getInt("pos");
				double precio = jugadoractual.getDouble("precio");
				double amarilla = jugadoractual.getDouble("chanceamarilla");
				double gol = jugadoractual.getDouble("chancegol");
				double asistencia = jugadoractual.getDouble("chanceasistencia");

				// creamos el jugador dependiendo si es jugador de campo o arquero
				if(pos == Constants.POS_ARQUERO) {
					jugador = new Arquero(nombreJugador, nombreEquipo, precio, amarilla, precio / 100, gol);
				} else {
					jugador = new Campo(nombreJugador, nombreEquipo, precio, amarilla, precio / 100, gol, asistencia, pos);
				}
				
				// agregamos el jugador al array
				jugadores.add(jugador);

			}

			// creamos el equipo con el arreglo de jugadores y los datos extraidos del JSON
			try {
				equipos.add(new EquipoDefault(nombreEquipo, grupo, jugadores));
			} catch (IncompleteTeamException e) {
				e.printStackTrace();
			}
		}

		return equipos;
	}
}
