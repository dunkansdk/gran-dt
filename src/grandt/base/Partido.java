package grandt.base;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import grandt.base.equipos.EquipoDefault;
import grandt.base.exceptions.NonSimulatedMatchException;
import grandt.base.jugador.Arquero;
import grandt.base.jugador.Campo;
import grandt.base.jugador.Jugador;
import grandt.base.jugador.JugadorData;
import grandt.utils.Constants;

/**
 * <p>Clase que almacena los datos de un partido determinado.</p>
 */
public class Partido {
	
	private EquipoDefault local;
	private EquipoDefault visitante;
	private ArrayList<JugadorData> stats;
	private String grupo;
	private LocalDate dia;
	
	private boolean simulated; // ya se simulo?
	
	public Partido(EquipoDefault local, EquipoDefault visitante, String grupo, LocalDate dia) 
	{
		this.local = local;
		this.visitante = visitante;
		this.grupo = grupo;
		this.dia = dia;
		this.simulated = false; // por defecto al instanciar la clase SIEMPRE va a ser false
		stats = new ArrayList<JugadorData>();
	}

	public EquipoDefault getLocal() 
	{
		return local;
	}

	public EquipoDefault getVisitante() 
	{
		return visitante;
	}

	/**
	 * <p>Retorna las estadisticas de los jugadores en caso de que se haya hecho la simulacion del partido</p>
	 * @return ({@link java.util.ArrayList ArrayList[JugadorData]}) el arreglo de las estadisticas de jugadores
	 * @throws NonSimulatedMatchException
	 * @see grandt.base.jugador.JugadorData
	 */
	public ArrayList<JugadorData> getStats() throws NonSimulatedMatchException
	{
		// si no se simulo, no podemos devolver nada (no hay estadisticas disponibles)
		if(!simulated) throw new NonSimulatedMatchException();
		return stats;
	}
	
	public String getGrupo()
	{
		return grupo;
	}
	
	public LocalDate getDia()
	{
		return dia;
	}
	
	/**
	 * <p>Funcion auxiliar que redondea un double a "places" decimales</p>
	 * @param value			&emsp;(<b>double</b>) el numero a redondear
	 * @param places		&emsp; (<b>int</b>) cantidad de decimales
	 * @return (<b>double</b>) numero redondeado
	 */
	private double round(double value, int places) 
	{
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	/**
	 * <p>Busca las estadisticas de un jugador y las retorna su indice</p>
	 * @param nombre				&emsp;(<b>String</b>) nombre del jugador
	 * @return	({@link {grandt.base.jugador.JugadorData JugadorData}) estadisticas del jugador solicitado
	 */
	private JugadorData buscarJugadorEnStats(String nombre)
	{
		for(JugadorData jd : stats)
		{
			if(jd.getJugador().getNombre().compareTo(nombre) == 0)
				return jd;
		}
		
		return null;
	}
	
	/**
	 * <p>Simula el partido y setea las estadisticas en el atributo (arreglo) de tipo JugadorData</p>
	 * @see grandt.base.jugador.JugadorData
	 */
	public void simular()
	{
		//System.out.println("Equipo Local: ");
		simularEquipo(visitante, local);
		//simularEquipo(local, visitante);
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//System.out.println("Equipo Visitante: ");
		
		simularEquipo(local, visitante);
	}
	
	/**
	 * <p>Inicia los threads de simulacion y llama a los metodos correspondientes.</p>
	 * @param team		&emsp;({@link grandt.base.equipos.EquipoDefault EquipoDefault}) el equipo a simular
	 * @param rival 	&emsp;({@link grandt.base.equipos.EquipoDefault EquipoDefault}) el equipo rival (para acceder a los datos de los jugadores rivales cuando fuera necesario)
	 */
	private void simularEquipo(EquipoDefault team, EquipoDefault rival)
	{
		// inicializamos el jugadorData de cada jugador.
		for(Jugador jugador : team.getJugadores())
			stats.add(new JugadorData(jugador));
		
		
		// simulamos las faltas y amonestaciones en un thread
		Thread t1 = new Thread(new Runnable() 
		{
			public void run() {
				simularAmarilla(team);
			}
		});
		
		// iniciamos el thread
		t1.start();
		
		// simulamos los goles y situaciones en otro thread
		Thread t2 = new Thread(new Runnable() 
		{
			public void run() {
				simularGol(team, rival);
			}
		});

		// iniciamos el thread
		t2.start();
		
		// ya se simulo el partido
		simulated = true;
	}
	
	/**
	 * <p>Simula las faltas y asigna amarillas (y rojas) a los jugadores.</p>
	 * @param team		&emsp;({@link grandt.base.equipos.EquipoDefault EquipoDefault}) el equipo a simular
	 */
	private void simularAmarilla(EquipoDefault team)
	{
		int faltas = team.getFaltas() / Constants.FALTAS_MODIFIER;
		//System.out.println("Faltas cometidas: " + faltas);
		
		for(int i = 0; i < faltas; i++)
		{
			// agarramos random al jugador que cometio la falta. No consideramos al arquero porque si lo expulsan no podemos hacer el calculo de chance de gol :(
			int rand =  ThreadLocalRandom.current().nextInt(1, 11);
			Jugador protagonista = team.getJugador(rand);
			
			JugadorData data = buscarJugadorEnStats(protagonista.getNombre());
			
			while(protagonista == null || data.getExpulsion()) // si esta expulsado elegimos otro
			{
				rand = ThreadLocalRandom.current().nextInt(1, 11);
				protagonista = team.getJugador(rand);
				data = buscarJugadorEnStats(protagonista.getNombre());
			}
			
			// una vez que tenemos el jugador, vemos si fue amonestado o no
			int chance = ThreadLocalRandom.current().nextInt(0, 100);
			if(protagonista.getChanceAmarilla() > chance)
			{
				// nos fijamos si ya tiene amarilla
				if(data != null)
				{
					if(data.getAmarilla())
					{
						data.setExpulsado(true);
						//System.out.println("DOBLE AMARILLA: " + protagonista.getNombre());
					}
					else
					{
						data.setAmonestado(true);
						//System.out.println("AMARILLA: " + protagonista.getNombre());
					}
				}
			}

			// pausamos un poco el thread para dar tiempo al otro
			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}
	
	/**
	 * <p>Simula las situaciones de gol (y los goles) de un equipo</p>
	 * @param team 		&emsp;({@link grandt.base.equipos.EquipoDefault EquipoDefault}) el equipo a simular
	 * @param rival		&emsp;({@link grandt.base.equipos.EquipoDefault EquipoDefault}) el equipo rival (para acceder a los datos del arquero)
	 */
	private void simularGol(EquipoDefault team, EquipoDefault rival)
	{
		int situaciones = team.getRating() / Constants.SITUACION_GOL_MODIFIER;
		//System.out.println("Equipo: " + team.getNombre() + ", Situaciones: " + situaciones);
		
		// simulamos las situaciones de gol
		for(int i = 0; i < situaciones; i++)
		{
			// elegimos un jugador aleatoriamente (no el arquero) que sera protagonista de la situacion de gol
			int rand = ThreadLocalRandom.current().nextInt(1, 11);
			Jugador protagonista = team.getJugador(rand);
			Arquero arqueroRival = rival.getArquero();
			
			JugadorData data = buscarJugadorEnStats(protagonista.getNombre());
					
			//System.out.println("SITUACION: ");
			
			while(protagonista == null || protagonista instanceof Arquero || data.getExpulsion()) // si esta expulsado elegimos otro
			{
				rand = ThreadLocalRandom.current().nextInt(1, 11);
				protagonista = team.getJugador(rand);
				data = buscarJugadorEnStats(protagonista.getNombre());
			}
			
			Campo protagonistaCampo = (Campo) protagonista;

			//System.out.println("\tJugador: " + protagonista.getNombre() + " chance de arco: " + protagonistaCampo.getChanceGol() + "%");
			//System.out.println("\tChance neta de gol: " + round((protagonistaCampo.getChanceGol() / 100 * arqueroRival.getChanceGol() / 100) * 100, 2) + "%");
			
			// vemos si la pelota "va al arco"
			int randomChance = ThreadLocalRandom.current().nextInt(0, 100);
			if(protagonistaCampo.getChanceGol() > randomChance)
			{
				// va al arco, chequeamos si el arquero rival no la ataja
				//System.out.println("\tChance de que la ataje: " + (100 - arqueroRival.getChanceGol()) + "%");
				
				int randomChanceArquero = ThreadLocalRandom.current().nextInt(0, 100);
				if(arqueroRival.getChanceGol() > randomChanceArquero)
				{
					// editamos el JugadorData del goleador
					if(data != null)
						data.meterGol();
					
					// gol
					//System.out.println("\tGOL");
					
					// vemos si alguien hizo la asistencia o si fue jugada propia
					int j = 0;
					boolean asistio = false;
					
					while(j < team.getJugadores().size() && !asistio)
					{
						Jugador asistidor = team.getJugador(j);
						
						if(asistidor instanceof Campo && asistidor.getNombre().compareTo(protagonista.getNombre()) != 0)
						{
							Campo asisCampo = (Campo) asistidor;
							
							// vemos si asistio
							randomChance = ThreadLocalRandom.current().nextInt(0, 100);
							if(asisCampo.getChanceAsistencia() > randomChance)
							{
								// asistio
								//System.out.println("\tASISTENCIA: " + asisCampo.getNombre());
								
								// editamos el JugadorData del asistidor
								data = buscarJugadorEnStats(asisCampo.getNombre());
								
								if(data != null)
								{
									data.darAsistencia();
									asistio = true;
								}
							}
						}
						
						j++;
					}
					
				} else {
					// la atajo
					//System.out.println("\tATAJADO (" + arqueroRival.getNombre() + ")");
				}
			} else {
				//System.out.println("\tDESVIADO");
			}
			
			// pausamos un poco el thread para dar tiempo al otro
			try {
				Thread.sleep(4);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
				
			//System.out.println("");
		}
	}

	@Override
	public String toString() 
	{
		return "Partido [local=" + local + ", visitante=" + visitante + ", stats=" + ", fecha= " + dia
				+ stats + "]\n";
	}

}
