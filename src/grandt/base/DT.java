package grandt.base;

import grandt.base.equipos.EquipoDT;
import grandt.base.exceptions.CompleteTeamException;
import grandt.base.exceptions.ExistentPlayerException;
import grandt.base.exceptions.InsufficientFundsException;
import grandt.base.exceptions.InvalidFormationException;
import grandt.base.jugador.Arquero;
import grandt.base.jugador.Campo;
import grandt.base.jugador.Jugador;
import grandt.utils.Constants;

/**
 * <p>Clase est�tica que maneja todos los atributos del usuario que est� jugando.</p>
 * <p>(<i>el gran DT</i>)</p>
 */
public class DT {
	
	private static final double DEFAULT_PRESUPUESTO = 20000.00;	
	
	private static EquipoDT equipo;	
	private static int puntos;
	private static double presupuesto;
	private static Formacion formacion;
	
	public static void init()
	{
		puntos = 0;
		presupuesto = DEFAULT_PRESUPUESTO;
		formacion = new Formacion(4, 3, 3);
		equipo = new EquipoDT(Constants.NOMBRE_EQUIPO_DEFAULT);
	}
	
	/**
	 * <p>Agrega un jugador al equipo</p>
	 * @param jugador		&emsp;({@link grandt.base.jugador.Jugador Jugador}) el jugador que se quiere comprar
	 * @throws ExistentPlayerException
	 * @throws CompleteTeamException
	 * @throws InsufficientFundsException
	 * @throws InvalidFormationException
	 */
	public static void comprarJugador(Jugador jugador) throws 	ExistentPlayerException, 
																CompleteTeamException, 
																InsufficientFundsException, 
																InvalidFormationException
	{
		
		if(equipo.full()) 
		{
			throw new CompleteTeamException();
		}
		
		if(existeJugador(jugador)) 
		{
			throw new ExistentPlayerException();
		}
		
		if(presupuesto < jugador.getPrecio())
		{
			throw new InsufficientFundsException();
		}
		
		// a la InvalidFormationException hay que agregarle una mini-descripci�n
		if(jugador instanceof Arquero)
		{
			if (equipo.hayArquero()) throw new  InvalidFormationException("ya hay arquero");
		} else {
			Campo jc = (Campo) jugador;

			if(equipo.getJugadoresPos(jc.getPosicion()) >= formacion.getJugadoresPos(jc.getPosicion()))
			{
				throw new InvalidFormationException("no podes agregar mas jugadores en esa posicion");
			}
		}
		
		presupuesto -= jugador.getPrecio();
		equipo.agregarJugador(jugador);
		
	}
	
	/**
	 * <p>Vende <i>"devuelve"</i> un jugador del equipo del DT</p>
	 * @param jugador					&emsp;({@link grandt.base.jugador.Jugador Jugador}) el jugador a vender
	 * @throws ExistentPlayerException
	 */
	public static void venderJugador(Jugador jugador) throws ExistentPlayerException
	{
		
		if(jugador == null || !existeJugador(jugador)) {
			throw new ExistentPlayerException();
		} else {
			presupuesto += jugador.getPrecio();
			equipo.eliminarJugador(jugador);
		}
		
	}
	
	/**
	 * <p>Suma puntaje al total del DT.</p>
	 * @param pts	&emsp;(<b>int</b>) los puntos a sumar
	 */
	public static void sumarPuntos(int pts)
	{
		puntos += pts;
	}
	
	/**
	 * <p>Cambia el nombre del equipo del DT</p>
	 * @param s		&emsp;(<b>String</b>) nuevo nombre del equipo
	 */
	public static void cambiarNombreEquipo(String s)
	{
		equipo.setNombre(s);
	}
	
	/**
	 * <p>Cambia la formacion del equipo del DT</p>
	 * @param nueva		&emsp;({@link grandt.base.Formacion Formacion}) nueva formacion
	 */
	public static void cambiarFormacion(Formacion nueva)
	{
		formacion = nueva;
	}
	
	/**
	 * <p>Chequea si el equipo est� completo</p>
	 * @return	(<b>boolean</b>) true si el equipo est� completo
	 */
	public static boolean equipoCompleto()
	{
		return(equipo.full());
	}

	/**
	 * <p>Resetea el equipo del DT</p>
	 */
	public static void resetEquipo()
	{
		equipo.reset();
	}
	
	/**
	 * <p>Comprueba si existe un jugador</p>
	 * @param jugador		&emsp;({@link grandt.base.jugador.Jugador Jugador}) el jugador a buscar
	 * @return (<b>boolean</b>) true si existe
	 */
	public static boolean existeJugador(Jugador jugador) 
	{
		for(Jugador j : equipo.getJugadores()) {
			if(jugador.equals(j)) return true;
		}
		return false;
	}
	
	public static EquipoDT getEquipo() {
		return equipo;
	}

	public static double getPresupuesto() {
		return presupuesto;
	}
	
	public static int getPuntos() {
		return puntos;
	}
	
	public static Formacion getFormacion()
	{
		return formacion;
	}
		
	/**
	 * <p>Para debuggear</p>
	 */
	public static void mostrarDT()
	{
		System.out.println("Nombre del team: " + equipo.getNombre());
		System.out.println("Presupuesto: " + presupuesto);
		System.out.println("Formacion: " + formacion);
		System.out.println("Team: " + equipo);
	}
}
