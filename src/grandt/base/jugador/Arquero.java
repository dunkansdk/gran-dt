package grandt.base.jugador;

/**
 * <p>Subclase de Jugador que corresponde a cada Arquero.</p>
 */
public class Arquero extends Jugador {
	
	private double chanceGol; // chanceGol, en arquero, es la chance de que le hagan el gol (que no lo ataje)

	public Arquero(String nombre, String equipo, double precio, double amarilla, double calidad, double chanceGol) 
	{
		super(nombre, equipo, precio, amarilla, calidad);
		this.chanceGol = chanceGol;
	}

	public double getChanceGol() 
	{
		return chanceGol;
	}

	@Override
	public String toString() {
		return super.toString() + "Arquero [chanceGol=" + chanceGol + "]";
	}

	
}
