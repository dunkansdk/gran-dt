package grandt.base.jugador;

/**<p>Subclase de Jugador que corresponde a los jugadores de Campo.</p>
 */
public class Campo extends Jugador {
	
	private double chanceGol;
	private double chanceAsistencia;
	private int posicion;
	
	public Campo(String nombre, String equipo, double precio, double amarilla, double calidad, double chanceGol, double chanceAsistencia, int posicion) 
	{
		super(nombre, equipo, precio, amarilla, calidad);
		this.chanceGol = chanceGol;
		this.chanceAsistencia = chanceAsistencia;
		this.posicion = posicion;
	}

	public double getChanceGol() 
	{
		return chanceGol;
	}
	
	public double getChanceAsistencia() 
	{
		return chanceAsistencia;
	}

	public int getPosicion()
	{
		return posicion;
	}
	
	@Override
	public String toString() {
		return super.toString() + " Campo [chanceGol=" + chanceGol + ", chanceAsistencia=" + chanceAsistencia + "]";
	}

}
