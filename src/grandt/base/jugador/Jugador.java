package grandt.base.jugador;

/**
 * <p>Clase base donde se guardan los datos de todos los jugadores existentes en la base de
 * datos (JSON)</p>
 * @see grandt.base.jugador.Arquero Arquero
 * @see grandt.base.jugador.Campo Campo
 */
public class Jugador {
	
	protected String nombre;
	protected String equipo;
	protected double precio;
	protected double chanceAmarilla;
	protected double calidad;

	public Jugador(String nombre, String equipo, double precio, double amarilla, double calidad) 
	{
		this.nombre = nombre;
		this.equipo = equipo;
		this.precio = precio;
		this.chanceAmarilla = amarilla;
		this.calidad = calidad;
	}
	
	public String getNombre() 
	{
		return nombre;
	}

	public String getEquipo() 
	{
		return equipo;
	}

	public double getPrecio() 
	{
		return precio;
	}

	public double getChanceAmarilla() 
	{
		return chanceAmarilla;
	}

	public double getCalidad() 
	{
		return calidad;
	}
	
	@Override
	public boolean equals(Object o) {
		
		if(o != null && o instanceof Jugador)
			if(this.nombre.compareTo(((Jugador) o).getNombre()) == 0)
				return true;
		
		return false;
	}

	@Override
	public String toString() 
	{
		return "Jugador [nombre=" + nombre + ", equipo=" + equipo + ", precio=" + precio + ", chanceAmarilla="
				+ chanceAmarilla + ", calidad=" + calidad + "]";
	}
	
}
