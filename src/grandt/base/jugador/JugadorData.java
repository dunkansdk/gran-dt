package grandt.base.jugador;

import grandt.utils.Constants;

/**
 * <p>A diferencia de Jugador, esta clase guarda todos los datos de un jugador para un partido
 * determinado. Es decir, en un determinado instante, puede haber varias instancias de JugadorData que corresponden
 * al mismo jugador, en partidos distintos.</p>
 * @see grandt.base.jugador.Jugador
 */
public class JugadorData {
	
	Jugador jugador;
	private int puntaje;
	private int goles;
	private int asistencias;
	private boolean amarilla;
	private boolean expulsion;

	public JugadorData(Jugador jugador) 
    {
		this.jugador = jugador;
		this.puntaje = Constants.PUNTAJE_DEFAULT;
		this.goles = 0;
		this.amarilla = false;
		this.expulsion = false;
		this.asistencias = 0;
	}
	
	public void setPuntaje(int puntaje)
	{
		this.puntaje = puntaje;
	}
	
	public void meterGol()
	{
		goles++;
		puntaje += Constants.PUNTOS_GOL;
	}
	
	public void setAmonestado(boolean val)
	{
		amarilla = val;
		puntaje -= Constants.PUNTOS_AMARILLA;
	}
	
	public void setExpulsado(boolean val)
	{
		expulsion = val;
		puntaje -= Constants.PUNTOS_AMARILLA;
	}
	
	public void darAsistencia()
	{
		asistencias++;
		puntaje += Constants.PUNTOS_ASISTENCIA;
	}

	public Jugador getJugador() 
    {
		return jugador;
	}

	public int getPuntaje() 
	{
		return puntaje;
	}

	public int getGoles() 
	{
		return goles;
	}

	public boolean getAmarilla() 
	{
		return amarilla;
	}

	public boolean getExpulsion() 
	{
		return expulsion;
	}
	
	public int getAsistencias()
	{
		return asistencias;
	}

	@Override
	public String toString() 
	{
		return "[jugador=" + jugador.getNombre() + ", puntaje=" + puntaje + ", goles=" + goles + ", amarilla="
				+ amarilla + ", expulsion=" + expulsion + ", asistencias= " + asistencias + "]\n";
	}
	
}
