package grandt.base.estados;

/**
 * Clase encargada de decidir qu� ventana (JFrame) se muestra en cada instante
 */
public class StateManager {

	private static IRenderizable currState; // todos los "estados" implementan la interfaz IRenderizable
	
	/**
	 * <p>Inicializa la clase est�tica adjudic�ndole un "estado" actual</p>
	 */
	public static void init()
	{
		// como estado "default" consideramos al estado "CreandoEquipo"
		currState = new StateCreandoEquipo();
		currState.create();
	}
	
	/**
	 * <p>Cambia el "estado" actual a un nuevo "estado" recibido por par�metro</p>
	 * @param newState		&emsp;({@link grandt.base.estados.IRenderizable IRenderizable}) el nuevo "estado"
	 */
	public static void switchState(IRenderizable newState)
	{
		currState.end();
		newState.create();
		currState = newState;
	}
	
	// PARA EL STATEJUGANDO SOLAMENTE
	public static void addMessageJugadores(String message)
	{
		if(currState instanceof StateJugando)
		{
			StateJugando cs = (StateJugando) currState;
			cs.addMessageJugadores(message);
		}
	}
	
	public static void addMessageResultados(String message)
	{
		if(currState instanceof StateJugando)
		{
			StateJugando cs = (StateJugando) currState;
			cs.addMessageResultados(message);
		}
	}
	// PARA EL STATEJUGANDO SOLAMENTE
}
