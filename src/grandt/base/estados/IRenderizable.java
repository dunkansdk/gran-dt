package grandt.base.estados;

/**
 * <p>Interfaz que implementan todos los "estados" que se dibujan en un JFrame</p>
 */
public interface IRenderizable {

	/**
	 * <p>Creación del estado (seteo de parámetros del JFrame)</p>
	 */
	void create();
	
	/**
	 * <p>Finalización del estado (dispose y otros)</p>
	 */
	void end();
}
