package grandt.base.estados;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import grandt.base.DT;
import grandt.base.Tabla;
import grandt.base.Torneo;
import grandt.base.equipos.EquipoData;
import grandt.base.estados.auxiliar.CanchaDisplay;
import grandt.base.estados.auxiliar.LFrame;
import grandt.base.estados.auxiliar.VentanaCruces;
import grandt.base.stats.Stats;
import grandt.utils.CommonUtils;
import grandt.utils.Constants;

/**
 * <p>Clase que representa al "estado" jugando, con su interfaz gr�fica.</p>
 * @see grandt.base.estados.IRenderizable IRenderizable
 */
public class StateJugando implements IRenderizable {

	// el frame
	private JFrame frame;
	
	// los textos de la pantalla "principal" del estado
	private JTextArea textAreaConsola;
	private JTextArea textAreaResultados;
	
	// texto y frame para la ventana de tabla de posiciones
	private JTextArea textTablas;
	private JFrame ventanaTablas;
	
	// ver clase LFrame en package "auxiliar"; son las sub-ventanas que comparten comportamiento
	private LFrame goleadores;
	private LFrame asistidores;
	private LFrame amarillas;
	
	// widgets adicionales de la interfaz
	private JLabel lblPuntaje;
	private JButton btnPasarFecha;
	private JButton btnSimularFecha;
	
	// ventana de cruces
	private VentanaCruces ventanaCruces;
	
	@Override
	public void create()
	{
		// inicializamos el frame y seteamos los atributos
		frame = new JFrame();
		frame.setTitle("Gran DT Rusia 2018");
		frame.setBounds(100, 100, 920, 540);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		// inicializamos las sub-ventanas
		goleadores = new LFrame("Tabla de Goleadores");
		asistidores = new LFrame("Tabla de Asistidores");
		amarillas = new LFrame("Tabla de Amarillas");
		
		// TABLA DE POSICIONES
		ventanaTablas = new JFrame("Tabla de Posiciones");	
		ventanaTablas.setResizable(false);
		ventanaTablas.setBounds(100, 100, 400, 400);
		ventanaTablas.setLocationRelativeTo(null);
		ventanaTablas.getContentPane().setLayout(null);

		textTablas = new JTextArea();
		textTablas.setEditable(false);
		
		JScrollPane scrollTablas = new JScrollPane(textTablas);
		scrollTablas.setBounds(13, 11, 368, 350);
		ventanaTablas.getContentPane().add(scrollTablas);
		// TABLA DE POSICIONES
		
		// cargamos los jugadores del DT en la lista "list"
		String data[] = new String[11]; 
		for(int i = 0; i < DT.getEquipo().getJugadores().size(); i++)
		{
			data[i] = DT.getEquipo().getJugadores().get(i).getNombre();
		}
		
		// creamos la lista de jugadores del DT y le asignamos su scrollPane
		JList list = new JList(data);
		
		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setBounds(29, 46, 174, 207);
		
		// aregamos el scrollPanel de la lista al frame
		frame.getContentPane().add(scrollPane);
		
		// inicializamos la "consola" principal de la ventana
		textAreaConsola = new JTextArea();
		textAreaConsola.setEditable(false);
		textAreaConsola.setText("Simule una fecha...");
		
		// le asignamos un scrollPane a la "consola"
		JScrollPane textAreaScroll = new JScrollPane(textAreaConsola);
		textAreaScroll.setBounds(236, 11, 442, 223);
		
		// agregamos el scrollPane de la consola al frame
		frame.getContentPane().add(textAreaScroll);
		
		// el label de la fecha actual (esquina superior-derecha)
		JLabel lblFechaActual = new JLabel("Fecha: Grupos - Fecha 1");
		lblFechaActual.setBounds(698, 13, 191, 32);
		
		// lo agregamos al frame
		frame.getContentPane().add(lblFechaActual);

		// creamos el bot�n que abre la tabla de posiciones
		JButton btnTablaPos = new JButton("Tabla de posiciones");
		btnTablaPos.setBounds(698, 93, 191, 23);

		// le asignamos comportamiento
		btnTablaPos.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mostrarTablas();
			}
			
		});
		
		// agregamos el bot�n al frame
		frame.getContentPane().add(btnTablaPos);
		
		// creamos el bot�n que pasa a la pr�xima fecha
		btnPasarFecha = new JButton("Proxima Fecha ->");
		btnPasarFecha.setEnabled(false);
		btnPasarFecha.setBounds(698, 265, 191, 23);
		
		// le asignamos comportamiento
		btnPasarFecha.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Torneo.pasarFecha();
				btnSimularFecha.setEnabled(true);
				btnPasarFecha.setEnabled(false);
				
				lblFechaActual.setText("Fecha: " + Torneo.getFechaActual());
				
				if(Torneo.hayCampeon())
				{
					JOptionPane.showMessageDialog(frame,
							"" + Torneo.getCampeon() + " CAMPEON! Hiciste " + DT.getPuntos() + " puntos.",
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
					btnSimularFecha.setEnabled(false);
				}
				
				if(Torneo.getOctavos().size() > 0)
					ventanaCruces.setOctavos(Torneo.getOctavos());
				if(Torneo.getCuartos().size() > 0)
					ventanaCruces.setCuartos(Torneo.getCuartos());
				if(Torneo.getSemifinales().size() > 0)
					ventanaCruces.setSemis(Torneo.getSemifinales());
				if(Torneo.getFinales().size() > 0)
					ventanaCruces.setFinal(Torneo.getFinales());
			}
			
		});
		
		// agregamos el bot�n al frame
		frame.getContentPane().add(btnPasarFecha);
		
		// creamos el bot�n que simula la fecha
		btnSimularFecha = new JButton("Simular fecha");
		btnSimularFecha.setBounds(698, 59, 191, 23);
		
		// le asignamos comportamiento
		btnSimularFecha.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textAreaConsola.setText("");
				textAreaResultados.setText("");
				btnSimularFecha.setEnabled(false);
				Torneo.simularFecha();
				lblPuntaje.setText("Puntaje: " + DT.getPuntos());
				btnPasarFecha.setEnabled(true);
			}
		});
		
		// aregamos el bot�n al frame
		frame.getContentPane().add(btnSimularFecha);
		
		// creamos el bot�n que abre la ventana de tabla de goleadores
		JButton btnTablaGoleadores = new JButton("Tabla de goleadores");
		btnTablaGoleadores.setBounds(698, 127, 191, 23);
		
		// le asignamos comportamiento
		btnTablaGoleadores.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				goleadores.create(Stats.getGoleadores());
			}
		});
		
		// lo agregamos al frame
		frame.getContentPane().add(btnTablaGoleadores);
		
		// creamos el bot�n que abre la ventana de tabla de asistidores
		JButton btnTablaDeAsistidores = new JButton("Tabla de asistidores");
		btnTablaDeAsistidores.setBounds(698, 161, 192, 23);
		
		// le asignamos comportamiento
		btnTablaDeAsistidores.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				asistidores.create(Stats.getAsistidores());
			}
		});
		
		// lo agregamos al frame
		frame.getContentPane().add(btnTablaDeAsistidores);
		
		// creamos el bot�n que abre la ventana de tabla de amarillas
		JButton btnTablaDeAmarilla = new JButton("Tabla de amarillas");
		btnTablaDeAmarilla.setBounds(697, 195, 192, 23);
		
		// le asignamos comportamiento
		btnTablaDeAmarilla.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				amarillas.create(Stats.getAmonestados());
			}
		});
		
		// lo agregamos al frame
		frame.getContentPane().add(btnTablaDeAmarilla);
		
		// creamos el label que muestra el nombre del equipo del DT
		JLabel lblEquipoDT = new JLabel(DT.getEquipo().getNombre());
		lblEquipoDT.setHorizontalAlignment(SwingConstants.CENTER);
		lblEquipoDT.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblEquipoDT.setBounds(29, 11, 174, 32);
		
		// lo agregamos al frame
		frame.getContentPane().add(lblEquipoDT);
		
		// creamos el display de la cancha
		CanchaDisplay cd = new CanchaDisplay(8, 330);
		cd.setLocation(10, 307);
		cd.setFormacion(DT.getFormacion());
		for(int j = 0; j < 4; j++)
		{
			for(int i = 0; i < DT.getFormacion().getJugadoresPos(j); i++)
			{
				if(j == Constants.POS_ARQUERO)
					cd.agregarArquero();
				else if(j == Constants.POS_DEFENSOR)
					cd.agregarDefensor();
				else if(j == Constants.POS_VOLANTE)
					cd.agregarVolante();
				else
					cd.agregarDelantero();
			}
		}
		
		// lo asignamos al frame
		frame.getContentPane().add(cd);
		
		// inicializamos el textAreaResultados
		textAreaResultados = new JTextArea("Simule una fecha...");

		// le agregamos un scrollPane
		JScrollPane scrollPaneResultados = new JScrollPane(textAreaResultados);
		scrollPaneResultados.setBounds(235, 263, 443, 223);
		
		// agregamos el scrollPane al frame
		frame.getContentPane().add(scrollPaneResultados);
		
		// creamos el label que muestra el puntaje del DT
		lblPuntaje = new JLabel("Puntaje: " + DT.getPuntos());
		lblPuntaje.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblPuntaje.setBounds(29, 264, 174, 14);
		
		// lo agregamos al frame
		frame.getContentPane().add(lblPuntaje);
		
		ventanaCruces = new VentanaCruces();
		
		JButton btnTablaDeCruces = new JButton("Tabla de cruces");
		btnTablaDeCruces.setBounds(697, 229, 192, 23);
		frame.getContentPane().add(btnTablaDeCruces);
		
		btnTablaDeCruces.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ventanaCruces.mostrar();
			}
			
		});
		
		// mostramos el frame
		frame.setVisible(true);
	}
	
	@Override
	public void end()
	{
		frame.dispose();
		frame = null;
	}
	
	public void addMessageJugadores(String msg)
	{
		textAreaConsola.append(msg + "\n");
	}
	
	public void addMessageResultados(String msg)
	{
		textAreaResultados.append(msg + "\n");
	}
	
	/**
	 * <p>Muestra la ventana co las tablas de posiciones de cada grupo (al abrirse, pullea los datos desde las {@link grandt.base.Tabla Tablas}
	 * en el {@link grandt.base.Torneo Torneo}</p>
	 */
	public void mostrarTablas()
	{
		textTablas.setText("");
		
		for(int i = 0; i < 8; i++)
		{
			
			textTablas.append("GRUPO " + CommonUtils.getGrupo(i) + "\n");
			Tabla t = Torneo.getTabla(CommonUtils.getGrupo(i));
			
			ArrayList<EquipoData> equipos = t.getEquipos();
			Collections.sort(equipos); // para esto EquipoData implementa Comparable
			
			for(EquipoData eq : equipos)
				textTablas.append("\t" + eq.getEquipo().getNombre() + ": " + eq.getPuntos() + ", DIF: " + eq.getDiferenciaGol() + "\n");
			
			textTablas.append("\n\n");
		}
		
		textTablas.setCaretPosition(0); // para scrollear arriba del todo
		ventanaTablas.setVisible(true);
	}
}
