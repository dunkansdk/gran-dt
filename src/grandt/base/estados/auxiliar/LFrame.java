package grandt.base.estados.auxiliar;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import grandt.utils.CommonUtils;

/**
 * <p>Clase auxiliar que engloba el funcionamiento de todas las subventanas del StateJugando</p>
 */
public class LFrame {
	
	// widgets comunes
	private JFrame frame;
	private JTextArea text;
	private JScrollPane scroll;
	
	public LFrame(String title) {
		// inicializamos el frame y asignamos atributos
		frame = new JFrame(title);	
		frame.setResizable(false);
		frame.setBounds(100, 100, 400, 400);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		// inicializamos el textArea
		text = new JTextArea();
		text.setEditable(false);
		
		// le asignamos un scrollPane
		scroll = new JScrollPane(text);
		scroll.setBounds(13, 11, 368, 350);
		
		// agregamos el scrollPane al frame
		frame.getContentPane().add(scroll);
	}
	
	/**
	 * <p>Crea la sub-ventana: carga de la clase {@link grandt.base.stats.Stats Stats} el <b>HashMap</b> necesario,
	 * lo ordena (las tablas se muestran ordenadas) y lo muestra en el textArea</p>
	 * @param input		&emsp;({@link java.util.HashMap HashMap[String, Integer])} alguno de los 3 mapas de la clase Stats (goleadores, amarillas o asistidores)
	 */
	@SuppressWarnings("rawtypes") // para que eclipse no moleste
	public void create(HashMap<String, Integer> input) {
		input = CommonUtils.sortByComparator(input, false);
		
		text.setText("");
		
		Iterator<Entry<String, Integer>> it = input.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        text.append("" + pair.getKey() + ": " + pair.getValue() + "\n");
	    }

	    text.setCaretPosition(0); // para scrollear arriba del todo
		frame.setVisible(true);
	}

}
