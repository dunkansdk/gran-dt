package grandt.base.estados.auxiliar;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import grandt.base.Formacion;
import grandt.utils.BufferedImageLoader;
import grandt.utils.Constants;

/**
 * <p>Display de la mini-cancha. Utilizamos un {@link java.awt.Canvas Canvas} donde dibujamos la imagen de la cancha 
 * (<i>data/resources/cancha.png</i>) de fondo, y encima vamos dibujando los "c�rculos" 
 * (<i>data/resources/cruz.png</i>, al principio eran cruces :P), en la posici�n que corresponda seg�n la formaci�n
 * y seg�n cu�ntos jugadores haya en esa posici�n.</p>
 */
public class CanchaDisplay extends Canvas {

	private static final long serialVersionUID = 1L;
	private BufferedImage imgFond;
	private BufferedImage cruz;
	
	private boolean hayArquero;
	private int cantDefensores;
	private int cantVolantes;
	private int cantDelanteros;
	
	private Formacion formacion;

    public CanchaDisplay(int x, int y) {
    	this.setBounds(x, y, 216, 172);
        imgFond = BufferedImageLoader.load("data/resources/cancha.png");
        cruz = BufferedImageLoader.load("data/resources/cruz.png");
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        
        if (imgFond != null) {
            g.drawImage(imgFond, 0, 0, this);
        }
        
        if(hayArquero)
        	g.drawImage(cruz, 216 / 2 - 8, 5, null);
        
        if(cantDefensores > 0)
        {
        	// el primero siempre se dibuja a la izquierda
        	g.drawImage(cruz, 25, 40, null);
        	
        	// proximo defensor
        	if(cantDefensores > 1)
        	{
        		if(formacion.getJugadoresPos(Constants.POS_DEFENSOR) == 3)
        		{
        			// si hay 3 defensores, el proximo se dibuja en el medio
        			g.drawImage(cruz, 216 / 2 - 8, 40, null);
        		} else {
        			g.drawImage(cruz, 216 / 2 - 8 - 30, 40, null);
        		}
        		
        		// proximo defensor
            	if(cantDefensores > 2)
            	{
            		if(formacion.getJugadoresPos(Constants.POS_DEFENSOR) == 3)
            		{
            			// si hay 3 defensores, el proximo se dibuja a la derecha
            			g.drawImage(cruz, 216 - 25 - 16, 40, null);
            		} else if (formacion.getJugadoresPos(Constants.POS_DEFENSOR) == 4) {
            			g.drawImage(cruz, 216 / 2 - 8 + 30, 40, null);
            		} else {
            			g.drawImage(cruz, 216 / 2 - 8, 40, null);
            		}
            		
            		// proximo defensor
                	if(cantDefensores > 3)
                	{
                		if(formacion.getJugadoresPos(Constants.POS_DEFENSOR) == 4)
                		{
                			g.drawImage(cruz, 216 - 25 - 16, 40, null);
                		} else {
                			g.drawImage(cruz, 216 / 2 - 8 + 30, 40, null);
                		}
                		
                		// proximo defensor
                    	if(cantDefensores > 4)
                    	{
                    		g.drawImage(cruz, 216 - 25 - 16, 40, null);
                    	}
                	}
            	}
        	}
        }
        
        if(cantVolantes > 0)
        {
        	// el primero siempre se dibuja a la izquierda
        	g.drawImage(cruz, 25, 80, null);
        	
        	// proximo volante
        	if(cantVolantes > 1)
        	{
        		if(formacion.getJugadoresPos(Constants.POS_VOLANTE) == 3)
        		{
        			g.drawImage(cruz, 216 / 2 - 8, 80, null);
        		} else {
        			g.drawImage(cruz, 216 / 2 - 8 - 30, 80, null);
        		}
        		
        		// proximo volante
            	if(cantVolantes > 2)
            	{
            		if(formacion.getJugadoresPos(Constants.POS_VOLANTE) == 3)
            		{
            			// si hay 3 defensores, el proximo se dibuja a la derecha
            			g.drawImage(cruz, 216 - 25 - 16, 80, null);
            		} else {
            			g.drawImage(cruz, 216 / 2 - 8 + 30, 80, null);
            		}
            		
            		// proximo volante
                	if(cantVolantes > 3)
                	{
                		g.drawImage(cruz, 216 - 25 - 16, 80, null);
                	}
            	}
        	}
        }
        
        if(cantDelanteros > 0)
        {
        	// el primero siempre se dibuja a la izquierda
        	g.drawImage(cruz, 25, 120, null);
        	
        	// proximo volante
        	if(cantDelanteros > 1)
        	{
        		if(formacion.getJugadoresPos(Constants.POS_DELANTERO) == 3)
        		{
        			g.drawImage(cruz, 216 / 2 - 8, 120, null);
        		} else {
        			g.drawImage(cruz, 216 - 25 - 16, 120, null);
        		}
        		
        		// proximo volante
            	if(cantDelanteros > 2)
            	{
            		g.drawImage(cruz, 216 - 25 - 16, 120, null);
            	}
        	}
        }
    }
    
    public void setFormacion(Formacion f)
    {
    	formacion = f;
    	
    	// reseteamos los atributos
    	hayArquero = false;
    	cantDefensores = 0;
    	cantVolantes = 0;
    	cantDelanteros = 0;
    }
	
    public void agregarArquero()
    {
    	hayArquero = true;
    }
    
    public void sacarArquero()
    {
    	hayArquero = false;
    }
    
    public void agregarDefensor()
    {
    	cantDefensores++;
    }
    
    public void sacarDefensor()
    {
    	cantDefensores--;
    	
    	if(cantDefensores < 0)
    		cantDefensores = 0;
    }
    
    public void agregarVolante()
    {
    	cantVolantes++;
    }
    
    public void sacarVolante()
    {
    	cantVolantes--;
    	
    	if(cantVolantes < 0)
    		cantVolantes = 0;
    }
    
    public void agregarDelantero()
    {
    	cantDelanteros++;
    }
    
    public void sacarDelantero()
    {
    	cantDelanteros--;
    	
    	if(cantDelanteros < 0)
    		cantDelanteros = 0;
    }
}
