package grandt.base.estados.auxiliar;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 * <p>Clase auxiliar para mostrar los cruces de las fases eliminatorias (muy fea :/)</p>
 */
public class VentanaCruces {

	private JFrame frame;
	private JTextArea textOctavosA;
	private JTextArea textCuartosA;
	private JTextArea textSemisA;
	private JTextArea textFinal;
	private JTextArea textSemisB;
	private JTextArea textCuartosB;
	private JTextArea textOctavosB;
	
	public VentanaCruces()
	{
		frame = new JFrame("Cruces");
		frame.setBounds(100, 100, 787, 460);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		
		textOctavosA = new JTextArea("PrimeroA\nSegundoB\n\n\n\n\n\n\n" + 
									 "PrimeroC\nSegundoD\n\n\n\n\n\n\n" + 
									 "PrimeroE\nSegundoF\n\n\n\n\n\n\n" +
									 "PrimeroG\nSegundoH\n\n\n\n\n\n\n");

		textOctavosA.setBackground(new Color(160,160,160));
		textOctavosA.setBounds(0, 0, 103, 424);
		textOctavosA.setEditable(false);
		frame.getContentPane().add(textOctavosA);
		textOctavosA.setColumns(10);
		
		textCuartosA = new JTextArea("\n\n\n\nCuartos1\nCuartos2" + "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nCuartos3\nCuartos4");
		textCuartosA.setBackground(new Color(160,160,160));
		textCuartosA.setColumns(10);
		textCuartosA.setBounds(113, 0, 103, 424);
		textCuartosA.setEditable(false);
		frame.getContentPane().add(textCuartosA);
		
		textSemisA = new JTextArea("\n\n\n\n\n\n\n\n\n\n\nSemis1\n\n\nSemis2");
		textSemisA.setBackground(new Color(160,160,160));
		textSemisA.setColumns(10);
		textSemisA.setBounds(226, 0, 103, 424);
		textSemisA.setEditable(false);
		frame.getContentPane().add(textSemisA);
		
		textFinal = new JTextArea("\n\n\n\n\n\n\n\n\n\n\n\nFinal1\nFinal2");
		textFinal.setBackground(new Color(160,160,160));
		textFinal.setColumns(10);
		textFinal.setBounds(339, 0, 103, 424);
		textFinal.setEditable(false);
		frame.getContentPane().add(textFinal);
		
		textSemisB = new JTextArea("\n\n\n\n\n\n\n\n\n\n\nSemis3\n\n\nSemis4");
		textSemisB.setBackground(new Color(160,160,160));
		textSemisB.setColumns(10);
		textSemisB.setBounds(452, 0, 103, 424);
		textSemisB.setEditable(false);
		frame.getContentPane().add(textSemisB);
		
		textCuartosB = new JTextArea("\n\n\n\nCuartos5\nCuartos6" + "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nCuartos7\nCuartos8");
		textCuartosB.setBackground(new Color(160,160,160));
		textCuartosB.setColumns(10);
		textCuartosB.setBounds(565, 0, 103, 424);
		textCuartosB.setEditable(false);
		frame.getContentPane().add(textCuartosB);
		
		textOctavosB = new JTextArea("PrimeroB\nSegundoA\n\n\n\n\n\n\n" + 
				 					 "PrimeroD\nSegundoC\n\n\n\n\n\n\n" + 
				 					 "PrimeroF\nSegundoE\n\n\n\n\n\n\n" +
				 					 "PrimeroH\nSegundoG\n\n\n\n\n\n\n");
		textOctavosB.setBackground(new Color(160,160,160));
		textOctavosB.setColumns(10);
		textOctavosB.setBounds(678, 0, 103, 424);
		textOctavosB.setEditable(false);
		frame.getContentPane().add(textOctavosB);
	}
	
	public void mostrar()
	{
		frame.setVisible(true);
	}
	
	public void setOctavos(ArrayList<String> octavos)
	{
		textOctavosA.setText(octavos.get(0) + "\n" + octavos.get(1) + "\n\n\n\n\n\n\n" 
						   + octavos.get(2) + "\n" + octavos.get(3) + "\n\n\n\n\n\n\n" 
						   + octavos.get(4) + "\n" + octavos.get(5) + "\n\n\n\n\n\n\n"
						   + octavos.get(6) + "\n" + octavos.get(7) + "\n\n\n\n\n\n\n");
		
		textOctavosA.setBackground(new Color(248, 222, 129));
		
		textOctavosB.setText(octavos.get(8) + "\n" + octavos.get(9) + "\n\n\n\n\n\n\n" 
				   		   + octavos.get(10) + "\n" + octavos.get(11) + "\n\n\n\n\n\n\n" 
				   		   + octavos.get(12) + "\n" + octavos.get(13) + "\n\n\n\n\n\n\n"
				   		   + octavos.get(14) + "\n" + octavos.get(15) + "\n\n\n\n\n\n\n");
		
		textOctavosB.setBackground(new Color(248, 222, 129));
	}
	
	public void setCuartos(ArrayList<String> cuartos)
	{
		textCuartosA.setText("\n\n\n\n" + cuartos.get(0) +  "\n" + cuartos.get(1) + "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" + cuartos.get(2) +"\n" + cuartos.get(3));
		textCuartosB.setText("\n\n\n\n" + cuartos.get(4) +  "\n" + cuartos.get(5) + "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" + cuartos.get(6) +"\n" + cuartos.get(7));
	
		textCuartosA.setBackground(new Color(248, 222, 129));
		textCuartosB.setBackground(new Color(248, 222, 129));
		
		textOctavosA.setBackground(new Color(236, 188, 111));
		textOctavosB.setBackground(new Color(236, 188, 111));
	}
	
	public void setSemis(ArrayList<String> semis)
	{
		textSemisA.setText("\n\n\n\n\n\n\n\n\n\n\n" + semis.get(0) + "\n\n\n" + semis.get(1));
		textSemisB.setText("\n\n\n\n\n\n\n\n\n\n\n" + semis.get(2) + "\n\n\n" + semis.get(3));
		
		textSemisA.setBackground(new Color(248, 222, 129));
		textSemisB.setBackground(new Color(248, 222, 129));
		
		textCuartosA.setBackground(new Color(236, 188, 111));
		textCuartosB.setBackground(new Color(236, 188, 111));
	}
	
	public void setFinal(ArrayList<String> fina)
	{
		textFinal.setText("\n\n\n\n\n\n\n\n\n\n\n\n" + fina.get(0) + "\n" + fina.get(1));
		
		textFinal.setBackground(new Color(248, 222, 129));

		textSemisA.setBackground(new Color(236, 188, 111));
		textSemisB.setBackground(new Color(236, 188, 111));
	}
}
