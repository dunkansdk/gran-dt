package grandt.base.estados;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.json.JSONObject;

import grandt.base.DT;
import grandt.base.Formacion;
import grandt.base.Torneo;
import grandt.base.equipos.EquipoDefault;
import grandt.base.estados.auxiliar.CanchaDisplay;
import grandt.base.exceptions.CompleteTeamException;
import grandt.base.exceptions.ExistentPlayerException;
import grandt.base.exceptions.InsufficientFundsException;
import grandt.base.exceptions.InvalidFormationException;
import grandt.base.jugador.Arquero;
import grandt.base.jugador.Campo;
import grandt.base.jugador.Jugador;
import grandt.utils.Constants;
import grandt.utils.JSONParser;

/**
 * <p>Estado "Creando Equipo", primer pantalla que se muestra al iniciar el juego.</p>
 */
public class StateCreandoEquipo implements IRenderizable {

	private CanchaDisplay canchaDisplay;
	
	// del equipo seleccionado
	private ArrayList<Jugador> jugadoresActuales;
	// del equipo del DT
	private ArrayList<Jugador> jugadoresDTActuales;
	
	// los widgets (son un par jajaja)
	private JFrame frame;
	private JList<String> listaJugadores;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JLabel lblNombre;
	private JLabel lblRating;
	private JLabel lblChanceGol;
	private JLabel lblChanceasis;
	private JLabel lblChanceamarilla;
	private JLabel lblPrecio;
	private JLabel lblPosi;
	private JComboBox<String> comboBox;
	private JScrollPane scrollPane_1;
	private JList<String> listaJugadoresDT; 
	private JButton btnAgregar;
	private JLabel lblPresupuesto;
	private JButton btnConfirmar;
	private JButton btnQuitar;
	private JComboBox<?> comboFormaciones;
	private JTextField txtNombre;
	
	private int currentFormacionIndex; // para el combobox de formaciones
	private int currentEquipoIndex; // por si me clickean a chile wn
	
	public StateCreandoEquipo()
	{
		jugadoresDTActuales = new ArrayList<Jugador>();
		canchaDisplay = new CanchaDisplay(273, 339);
		canchaDisplay.setFormacion(new Formacion(4, 3, 3));
		currentFormacionIndex = 0;
		currentEquipoIndex = 0;
	}
	
	@Override
	public void create()
	{
		frame = new JFrame();
		frame.setTitle("Gran DT Rusia 2018");
		frame.setBounds(100, 100, 515, 585);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		// seteamos a "Rusia" como el equipo actual (porque es el primero en la lista)
		EquipoDefault equipoActual = Torneo.getEquipo(Constants.ID_EQUIPO_RUSIA);
		jugadoresActuales = equipoActual.getJugadores();
		String data[] = new String[11];
		
		// actualizamos la "data" (String[] que se va a mostrar en la lista)
		for(int i = 0; i < 11; i++)
			data[i] = jugadoresActuales.get(i).getNombre();
		
		listaJugadores = new JList<String>(data);
		
		// seteamos el primero como seleccionado
		listaJugadores.setSelectedIndex(0);

		scrollPane = new JScrollPane(listaJugadores);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(25, 127, 209, 177);
		
		frame.getContentPane().add(scrollPane);
		
		// INICIALIZACION DE WIDGETS
		panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		panel.setBounds(25, 315, 209, 124);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBounds(10, 0, 196, 14);
		panel.add(lblNombre);
		
		lblRating = new JLabel("Rating");
		lblRating.setHorizontalAlignment(SwingConstants.LEFT);
		lblRating.setForeground(Color.WHITE);
		lblRating.setBounds(10, 63, 196, 14);
		panel.add(lblRating);
		
		lblChanceGol = new JLabel("ChanceGol");
		lblChanceGol.setHorizontalAlignment(SwingConstants.LEFT);
		lblChanceGol.setForeground(Color.WHITE);
		lblChanceGol.setBounds(10, 78, 196, 14);
		panel.add(lblChanceGol);
		
		lblChanceasis = new JLabel("ChanceAsis");
		lblChanceasis.setHorizontalAlignment(SwingConstants.LEFT);
		lblChanceasis.setForeground(Color.WHITE);
		lblChanceasis.setBounds(10, 93, 196, 14);
		panel.add(lblChanceasis);
		
		lblChanceamarilla = new JLabel("ChanceAmarilla");
		lblChanceamarilla.setHorizontalAlignment(SwingConstants.LEFT);
		lblChanceamarilla.setForeground(Color.WHITE);
		lblChanceamarilla.setBounds(10, 108, 196, 14);
		panel.add(lblChanceamarilla);
		
		lblPrecio = new JLabel("Precio");
		lblPrecio.setHorizontalAlignment(SwingConstants.LEFT);
		lblPrecio.setForeground(Color.WHITE);
		lblPrecio.setBounds(10, 47, 196, 14);
		panel.add(lblPrecio);
		
		lblPosi = new JLabel("Posi100");
		lblPosi.setHorizontalAlignment(SwingConstants.LEFT);
		lblPosi.setForeground(Color.RED);
		lblPosi.setBounds(10, 22, 196, 14);
		panel.add(lblPosi);

		lblPresupuesto = new JLabel("Presupuesto: $" + DT.getPresupuesto());
		lblPresupuesto.setHorizontalAlignment(SwingConstants.CENTER);
		lblPresupuesto.setBounds(273, 99, 209, 14);
		frame.getContentPane().add(lblPresupuesto);
		// INICIALIZACION DE WIDGETS
		
		// actualizamos los labels para que no muestren basura
		lblNombre.setText("" + jugadoresActuales.get(0).getNombre());
    	lblPrecio.setText("Precio: $" + jugadoresActuales.get(0).getPrecio());
    	lblRating.setText("Rating: " + (double) (jugadoresActuales.get(0).getPrecio() / 100));
    	lblChanceamarilla.setText("Chance amarilla: " + jugadoresActuales.get(0).getChanceAmarilla() + "%");
    	
    	// seg�n la posici�n mostramos datos distintos
    	if(jugadoresActuales.get(0) instanceof Arquero)
    	{
    		Arquero arq = (Arquero) jugadoresActuales.get(0);
    		lblPosi.setText("Arquero");
    		lblChanceGol.setText("Chance gol: " + arq.getChanceGol() + "%");
    		lblChanceasis.setText("Chance asistencia: %");
    	} else {
    		Campo cmp = (Campo) jugadoresActuales.get(0);
    		
    		int pos = cmp.getPosicion();
    		
    		if(pos == Constants.POS_DEFENSOR)
    			lblPosi.setText("Defensor");
    		else if(pos == Constants.POS_VOLANTE)
    			lblPosi.setText("Volante");
    		else
    			lblPosi.setText("Delantero");
    		
    		lblChanceGol.setText("Chance gol: " + cmp.getChanceGol() + "%");
    		lblChanceasis.setText("Chance asistencia: " + cmp.getChanceAsistencia() + "%");
    	}
		
		String equipos[] = new String[33];
		
		// cargamos los nombres de los equipos desde el JSON "data"
		JSONObject equiposData = JSONParser.parse("data/data.json");
		int size = equiposData.getInt("num_equipos");
		JSONObject equiposJSON = equiposData.getJSONObject("equipos");
		for(int i = 0; i < size; i++)
		{
			JSONObject eqActualJSON = equiposJSON.getJSONObject(String.valueOf(i));
			equipos[i] = eqActualJSON.getString("nombre");
		}
		
		comboBox = new JComboBox<String>(equipos);
		comboBox.setBounds(25, 96, 209, 20);
		
		// agregamos el listener al comboBox para que actualice la lista al clickearlo
		ActionListener comboBoxActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int index = comboBox.getSelectedIndex(); //get the selected item
                String equipo = (String) comboBox.getSelectedItem();
                
                if(equipo.compareToIgnoreCase("Chile") == 0)
                {
                	JOptionPane.showMessageDialog(frame,
							"Ocurrio una wea inesperada ql",
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
                	
                	comboBox.setSelectedIndex(currentEquipoIndex);
                } else {
                	DefaultListModel<String> model = new DefaultListModel<String>();
                    model.clear();
                    
                    EquipoDefault newEquipo = Torneo.getEquipo(index);
                    jugadoresActuales = newEquipo.getJugadores();
                    
                    for(int i = 0; i < 11; i++)
                    	model.addElement(jugadoresActuales.get(i).getNombre());
      
                    listaJugadores.setModel(model);
                    currentEquipoIndex = comboBox.getSelectedIndex();
                }
            }
        };
        comboBox.addActionListener(comboBoxActionListener);
		
        // agregamos el listener a la lista para que actualice los labels con la informacion del jugador seleccionado
        ListSelectionListener listBoxActionListener = new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting() == false) {
                	int i = listaJugadores.getSelectedIndex();
                    if (i != -1) {
                    	
                    	lblNombre.setText("" + jugadoresActuales.get(i).getNombre());
                    	
                    	String equipo = (String) comboBox.getSelectedItem();

                    	// Jap�n es un meme =)
                    	if(equipo.compareToIgnoreCase("Japon") == 0)
                    	{
                    		lblPrecio.setText("Precio: ???");
                    		lblRating.setText("Rating: ???");
                    		lblChanceamarilla.setText("Chance amarilla: ???");
                    	} else {
                    		lblPrecio.setText("Precio: $" + jugadoresActuales.get(i).getPrecio());
                        	lblRating.setText("Rating: " + (double) (jugadoresActuales.get(i).getPrecio() / 100));
                        	lblChanceamarilla.setText("Chance amarilla: " + jugadoresActuales.get(i).getChanceAmarilla() + "%");
                    	}
                    	
                    	if(jugadoresActuales.get(i) instanceof Arquero)
                    	{
                    		Arquero arq = (Arquero) jugadoresActuales.get(i);
                    		lblPosi.setText("Arquero");
                    		if(equipo.compareToIgnoreCase("Japon") == 0)
                        	{
                    			lblChanceGol.setText("Chance gol: ???");
                    			lblChanceasis.setText("Chance asistencia: ???");
                        	} else {
                        		lblChanceGol.setText("Chance gol: " + arq.getChanceGol() + "%");
                        		lblChanceasis.setText("Chance asistencia: 0%");
                        	}
                    	} else {
                    		Campo cmp = (Campo) jugadoresActuales.get(i);
                    		
                    		int pos = cmp.getPosicion();
                    		
                    		if(pos == Constants.POS_DEFENSOR)
                    			lblPosi.setText("Defensor");
                    		else if(pos == Constants.POS_VOLANTE)
                    			lblPosi.setText("Volante");
                    		else
                    			lblPosi.setText("Delantero");
                    		
                    		if(equipo.compareToIgnoreCase("Japon") == 0)
                        	{
                    			lblChanceGol.setText("Chance gol: ???");
                    			lblChanceasis.setText("Chance asistencia: ???");
                        	} else {
                        		lblChanceGol.setText("Chance gol: " + cmp.getChanceGol() + "%");
                        		lblChanceasis.setText("Chance asistencia: " + cmp.getChanceAsistencia() + "%");
                        	}
                    	}
                    }
                }
            }
        };
        
        listaJugadores.addListSelectionListener(listBoxActionListener);
        
		frame.getContentPane().add(comboBox);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(76, 443, 89, 23);
		
		// agregamos el listener al boton para que llame a comprarJugador()
		btnAgregar.addActionListener(new ActionListener() { 
				public void actionPerformed(ActionEvent e) { 
					if(!listaJugadores.isSelectionEmpty())
					{
						Jugador jugadorToBuy = jugadoresActuales.get(listaJugadores.getSelectedIndex());

						if(jugadorToBuy.getEquipo().compareTo("Japon") == 0)
						{
							JOptionPane.showMessageDialog(frame,
									"Ese jugador es demasiado bueno para tu equipo",
								    "Error",
								    JOptionPane.ERROR_MESSAGE);
						} else {
							comprarJugador(jugadorToBuy);
						}
						
					}
				} 
			} 
		);
		
		frame.getContentPane().add(btnAgregar);
		frame.getContentPane().add(canchaDisplay);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(200, 522, 113, 23);
		
		// agregamos el listener al boton de confirmar para que pase al siguiente estado
		btnConfirmar.addActionListener(new ActionListener() { 
				public void actionPerformed(ActionEvent e) { 
					if(!DT.equipoCompleto())
					{
						JOptionPane.showMessageDialog(frame,
								"Equipo incompleto",
							    "Error",
							    JOptionPane.ERROR_MESSAGE);
					} else {
						DT.cambiarNombreEquipo(txtNombre.getText());
						//DT.mostrarDT();
						
						StateManager.switchState(new StateJugando());
					}
				} 
			} 
		);
		
		frame.getContentPane().add(btnConfirmar);
		
		listaJugadoresDT = new JList<String>();

		scrollPane_1 = new JScrollPane(listaJugadoresDT);
		scrollPane_1.setBounds(273, 127, 209, 177);
		frame.getContentPane().add(scrollPane_1);
		
		btnQuitar = new JButton("Quitar");
		btnQuitar.setBounds(335, 315, 89, 23);
		
		// agregamos el listener al boton de quitar jugadores del equipo
		btnQuitar.addActionListener(new ActionListener() { 
				public void actionPerformed(ActionEvent e) { 
					if(!listaJugadoresDT.isSelectionEmpty())
					{
						Jugador aBorrar = jugadoresDTActuales.get(listaJugadoresDT.getSelectedIndex());

						if(aBorrar != null)
						{
							try {
								DT.venderJugador(aBorrar);
								
								// lo sacamos de la lista
								jugadoresDTActuales.remove(listaJugadoresDT.getSelectedIndex());
								
								DefaultListModel<String> model = new DefaultListModel<String>();
								
								for(Jugador s : jugadoresDTActuales)
									model.addElement(s.getNombre());
								
								listaJugadoresDT.setModel(model);
								
								// actualizamos el presupuesto
								lblPresupuesto.setText("Presupuesto: $" + DT.getPresupuesto());
								
								// actualizamos el display de la cancha
								if(aBorrar instanceof Arquero)
									canchaDisplay.sacarArquero();
								else {
									Campo jc = (Campo) aBorrar;
									
									if(jc.getPosicion() == Constants.POS_DEFENSOR)
										canchaDisplay.sacarDefensor();
									else if(jc.getPosicion() == Constants.POS_VOLANTE)
										canchaDisplay.sacarVolante();
									else if(jc.getPosicion() == Constants.POS_DELANTERO)
										canchaDisplay.sacarDelantero();
								}
								
								canchaDisplay.repaint();
								
							} catch (ExistentPlayerException e1) {
								JOptionPane.showMessageDialog(frame,
										"Ocurrio un error inesperado",
									    "Error",
									    JOptionPane.ERROR_MESSAGE);
							}
						}
					}
				} 
			} 
		);
		
		frame.getContentPane().add(btnQuitar);

		String[] formaciones = {"4-3-3", "4-4-2", "3-4-3", "5-3-2"}; // las formaciones validas (medio hardcodeadas :/)
		comboFormaciones = new JComboBox<Object>(formaciones);
		comboFormaciones.setBounds(230, 65, 55, 20);
		comboFormaciones.setSelectedIndex(currentFormacionIndex);
		
		// agregamos el listener al comboBox de formaciones para que cambie la formacion del equipo del DT
		ActionListener formacionesComboListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            	int dialogResult = JOptionPane.showConfirmDialog(null, "Cambiar la formacion borrara el equipo... Desea seguir?", "Warning", JOptionPane.OK_CANCEL_OPTION);
            	
            	if(dialogResult == JOptionPane.YES_OPTION){
            		
            		// limpiamos la lista de jugadores del DT
            		DefaultListModel<String> model = new DefaultListModel<String>();
                    model.clear();
                    listaJugadoresDT.setModel(model);
                    
                    // reseteamos el equipo del DT
                    jugadoresDTActuales.clear();
                    DT.resetEquipo();
                    
                    // cambiamos la formacion del DT
                    String formacionActual = (String) comboFormaciones.getSelectedItem();
					
					if(formacionActual.compareTo("4-3-3") == 0)
					{
						DT.cambiarFormacion(new Formacion(4, 3, 3));
						canchaDisplay.setFormacion(new Formacion(4, 3, 3));
					}
					else if(formacionActual.compareTo("4-4-2") == 0)
					{
						DT.cambiarFormacion(new Formacion(4, 4, 2));
						canchaDisplay.setFormacion(new Formacion(4, 4, 2));
					}
					else if(formacionActual.compareTo("3-4-3") == 0)
					{
						DT.cambiarFormacion(new Formacion(3, 4, 3));
						canchaDisplay.setFormacion(new Formacion(3, 4, 3));
					}
					else 
					{
						DT.cambiarFormacion(new Formacion(5, 3, 2));
						canchaDisplay.setFormacion(new Formacion(5, 3, 2));
					}

					canchaDisplay.repaint();
                    currentFormacionIndex = comboFormaciones.getSelectedIndex();
            	} else {
            		comboFormaciones.setSelectedIndex(currentFormacionIndex);
            	}
            }
        };
        
        comboFormaciones.addActionListener(formacionesComboListener);
		
		frame.getContentPane().add(comboFormaciones);
		
		JLabel lblNombreDelEquipo = new JLabel("Nombre del Equipo");
		lblNombreDelEquipo.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreDelEquipo.setBounds(0, 11, 509, 14);
		frame.getContentPane().add(lblNombreDelEquipo);
		
		txtNombre = new JTextField();
		txtNombre.setHorizontalAlignment(SwingConstants.CENTER);
		txtNombre.setText(Constants.NOMBRE_EQUIPO_DEFAULT);
		txtNombre.setBounds(176, 34, 163, 20);
		frame.getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		frame.setVisible(true);
	}
	
	@Override
	public void end()
	{
		frame.dispose();
		frame = null;
	}

	/**
	 * <p>Funci�n auxiliar para modularizar un poco; actualiza la lista de jugadores y llama a 
	 * {@link grandt.base.DT#comprarJugador(Jugador) DT.comprarJugador(Jugador)}</p>
	 * @param jugadorToBuy
	 */
	private void comprarJugador(Jugador jugadorToBuy)
	{
		try {
			DT.comprarJugador(jugadorToBuy);

			jugadoresDTActuales.add(jugadorToBuy);
			
			DefaultListModel<String> model = new DefaultListModel<String>();
			
			for(Jugador s : jugadoresDTActuales)
				model.addElement(s.getNombre());
			
			listaJugadoresDT.setModel(model);
			
			// actualizamos el presupuesto
			lblPresupuesto.setText("Presupuesto: $" + DT.getPresupuesto());
			
			// actualizamos el display de la cancha
			if(jugadorToBuy instanceof Arquero)
				canchaDisplay.agregarArquero();
			else {
				Campo jc = (Campo) jugadorToBuy;
				
				if(jc.getPosicion() == Constants.POS_DEFENSOR)
					canchaDisplay.agregarDefensor();
				else if(jc.getPosicion() == Constants.POS_VOLANTE)
					canchaDisplay.agregarVolante();
				else if(jc.getPosicion() == Constants.POS_DELANTERO)
					canchaDisplay.agregarDelantero();
			}
			
			canchaDisplay.repaint();
			
		} catch (ExistentPlayerException | CompleteTeamException | InsufficientFundsException | InvalidFormationException e1) {
			JOptionPane.showMessageDialog(frame,
					e1.getMessage(),
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
		}
	}
}

