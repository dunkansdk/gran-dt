package grandt.base;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import grandt.base.equipos.EquipoData;
import grandt.base.equipos.EquipoDefault;
import grandt.base.estados.StateManager;
import grandt.base.exceptions.NonSimulatedMatchException;
import grandt.base.fechas.Fecha;
import grandt.base.fechas.FechaEliminatoria;

/**
 * <p>Clase "maestra" de la aplicacion; determina las acciones del programa.</p>
 */
public class Torneo {
	
	private static ArrayList<Fecha> fechas;
	private static ArrayList<EquipoDefault> equipos;
	private static HashMap<String, Tabla> tablasGrupos; // <Grupo, Tabla>
	private static Fecha fechaActual = null;
	private static int fechaActualNum; // para llevar seguimiento de las fechas a simular
	
	// las fechas eliminatorias (para mostrarlas en los cruces)
	private static ArrayList<String> octavos;
	private static ArrayList<String> cuartos;
	private static ArrayList<String> semifinales;
	private static ArrayList<String> finales;
	
	private static String campeon; // para cuando ya termino el mundial
	
	private static final int FASE_GRUPOS_FECHA_1 = 0;
	private static final int FASE_GRUPOS_FECHA_2 = 1;
	private static final int FASE_GRUPOS_FECHA_3 = 2;
	private static final int FASE_OCTAVOS = 3;
	private static final int FASE_CUARTOS = 4;
	private static final int FASE_SEMIS = 5;
	private static final int FASE_FINAL = 6;
	private static final int FASE_TERMINO = 7;
	
	/**
	 * <p>Inicializa el torneo (setea arreglos)</p>
	 */
	public static void init()
	{
		fechas = new ArrayList<Fecha>();
		equipos = new ArrayList<EquipoDefault>();
		tablasGrupos = new HashMap<String, Tabla>();
		octavos = new ArrayList<String>();
		cuartos = new ArrayList<String>();
		semifinales = new ArrayList<String>();
		finales = new ArrayList<String>();
		fechaActualNum = FASE_GRUPOS_FECHA_1;
		campeon = "NONE";
	}
	
	/**
	 * <p>Carga el arreglo de equipos del torneo</p>
	 * @param teams		&emsp;({@link java.util.ArrayList ArrayList[EquipoDefault]})
	 * @see grandt.base.equipos.EquipoDefault EquipoDefault
	 */
	public static void loadEquipos(ArrayList<EquipoDefault> teams)
	{
		equipos = teams;
		cargarTablas();
	}
	
	private static void cargarTablas()
	{
		// creamos las tablas de la fase de grupos
		ArrayList<EquipoData> eqs = new ArrayList<EquipoData>();
		for(int i = 1; i <= equipos.size(); i++)
		{
			EquipoData curr = new EquipoData(equipos.get(i-1));
			eqs.add(curr);
			
			if(i % 4 == 0)
			{
				tablasGrupos.put(equipos.get(i-1).getGrupo(), new Tabla(eqs));
				eqs = new ArrayList<EquipoData>();
			}
		}
	}
	
	public static Tabla getTabla(String grupo)
	{
		return tablasGrupos.get(grupo);
	}
	
	/**
	 * <p>Carga una fecha al arreglo de fechas</p>
	 * @param fecha		&emsp;({@link grandt.base.fechas.Fecha Fecha}) la fecha a agregar
	 */
	public static void addFecha(Fecha fecha)
	{
		fechas.add(fecha);
	}
	
	public static void simularFecha()
	{
		if(fechaActual == null) fechaActual = fechas.get(0);
		fechaActual.simular();
	}
	
	public static void pasarFecha()
	{
		fechaActualNum++;
		
		switch(fechaActualNum)
		{
			case FASE_GRUPOS_FECHA_2: case FASE_GRUPOS_FECHA_3:
				fechaActual = fechas.get(fechaActualNum);
				break;
				
			case FASE_OCTAVOS:
				fechaActual = generarFechaOctavos();
				break;
				
			case FASE_CUARTOS:
				try {
					fechaActual = generarCuartos((FechaEliminatoria) fechaActual);
				} catch (NonSimulatedMatchException e) {
					e.printStackTrace();
				}
				break;
				
			case FASE_SEMIS:
				try {
					fechaActual = generarSemifinal((FechaEliminatoria) fechaActual);
				} catch (NonSimulatedMatchException e) {
					e.printStackTrace();
				}
				break;
				
			case FASE_FINAL:
				try {
					fechaActual = generarFinal((FechaEliminatoria) fechaActual);
				} catch (NonSimulatedMatchException e) {
					e.printStackTrace();
				}
				break;
				
			case FASE_TERMINO:
				FechaEliminatoria fechaFinal = (FechaEliminatoria) fechaActual;
				try {
					campeon = fechaFinal.getGanadores().get(0).getNombre();
				} catch (NonSimulatedMatchException e) {
					e.printStackTrace();
				}
				break;
				
			default:
				break;
		}
	}

	private static FechaEliminatoria generarFechaOctavos()
	{
		ArrayList<Partido> partidos = new ArrayList<Partido>();
		partidos.add(new Partido(tablasGrupos.get("A").primero().getEquipo(), tablasGrupos.get("B").segundo().getEquipo(), "OCTAVOS", LocalDate.of(2018, 6, 30)));
		partidos.add(new Partido(tablasGrupos.get("C").primero().getEquipo(), tablasGrupos.get("D").segundo().getEquipo(), "OCTAVOS", LocalDate.of(2018, 6, 30)));
		partidos.add(new Partido(tablasGrupos.get("E").primero().getEquipo(), tablasGrupos.get("F").segundo().getEquipo(), "OCTAVOS", LocalDate.of(2018, 7, 2)));
		partidos.add(new Partido(tablasGrupos.get("G").primero().getEquipo(), tablasGrupos.get("H").segundo().getEquipo(), "OCTAVOS", LocalDate.of(2018, 7, 2)));
		partidos.add(new Partido(tablasGrupos.get("B").primero().getEquipo(), tablasGrupos.get("A").segundo().getEquipo(), "OCTAVOS", LocalDate.of(2018, 7, 1)));
		partidos.add(new Partido(tablasGrupos.get("D").primero().getEquipo(), tablasGrupos.get("C").segundo().getEquipo(), "OCTAVOS", LocalDate.of(2018, 7, 1)));
		partidos.add(new Partido(tablasGrupos.get("F").primero().getEquipo(), tablasGrupos.get("E").segundo().getEquipo(), "OCTAVOS", LocalDate.of(2018, 7, 3)));
		partidos.add(new Partido(tablasGrupos.get("H").primero().getEquipo(), tablasGrupos.get("G").segundo().getEquipo(), "OCTAVOS", LocalDate.of(2018, 7, 3)));

		octavos.add(tablasGrupos.get("A").primero().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("B").segundo().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("C").primero().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("D").segundo().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("E").primero().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("F").segundo().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("G").primero().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("H").segundo().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("A").segundo().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("B").primero().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("C").segundo().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("D").primero().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("E").segundo().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("F").primero().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("G").segundo().getEquipo().getNombre());
		octavos.add(tablasGrupos.get("H").primero().getEquipo().getNombre());
		
		return new FechaEliminatoria(partidos);
	}
	
	private static FechaEliminatoria generarCuartos(FechaEliminatoria fecha) throws NonSimulatedMatchException
	{
		ArrayList<Partido> partidos = new ArrayList<Partido>();
		partidos.add(new Partido(fecha.getGanadores().get(0), fecha.getGanadores().get(1), "CUARTOS", LocalDate.of(2018, 6, 30)));
		partidos.add(new Partido(fecha.getGanadores().get(2), fecha.getGanadores().get(3), "CUARTOS", LocalDate.of(2018, 6, 30)));
		partidos.add(new Partido(fecha.getGanadores().get(4), fecha.getGanadores().get(5), "CUARTOS", LocalDate.of(2018, 6, 30)));
		partidos.add(new Partido(fecha.getGanadores().get(6), fecha.getGanadores().get(7), "CUARTOS", LocalDate.of(2018, 6, 30)));
		
		cuartos.add(fecha.getGanadores().get(0).getNombre());
		cuartos.add(fecha.getGanadores().get(1).getNombre());
		cuartos.add(fecha.getGanadores().get(2).getNombre());
		cuartos.add(fecha.getGanadores().get(3).getNombre());
		cuartos.add(fecha.getGanadores().get(4).getNombre());
		cuartos.add(fecha.getGanadores().get(5).getNombre());
		cuartos.add(fecha.getGanadores().get(6).getNombre());
		cuartos.add(fecha.getGanadores().get(7).getNombre());
		
		return new FechaEliminatoria(partidos);
	}
	
	private static FechaEliminatoria generarSemifinal(FechaEliminatoria fecha) throws NonSimulatedMatchException
	{
		ArrayList<Partido> partidos = new ArrayList<Partido>();
		partidos.add(new Partido(fecha.getGanadores().get(0), fecha.getGanadores().get(1), "CUARTOS", LocalDate.of(2018, 6, 30)));
		partidos.add(new Partido(fecha.getGanadores().get(2), fecha.getGanadores().get(3), "CUARTOS", LocalDate.of(2018, 6, 30)));
		
		semifinales.add(fecha.getGanadores().get(0).getNombre());
		semifinales.add(fecha.getGanadores().get(1).getNombre());
		semifinales.add(fecha.getGanadores().get(2).getNombre());
		semifinales.add(fecha.getGanadores().get(3).getNombre());
		
		return new FechaEliminatoria(partidos);
	}
	
	private static FechaEliminatoria generarFinal(FechaEliminatoria fecha) throws NonSimulatedMatchException
	{
		ArrayList<Partido> partidos = new ArrayList<Partido>();
		partidos.add(new Partido(fecha.getGanadores().get(0), fecha.getGanadores().get(1), "CUARTOS", LocalDate.of(2018, 6, 30)));
		
		finales.add(fecha.getGanadores().get(0).getNombre());
		finales.add(fecha.getGanadores().get(1).getNombre());
		
		return new FechaEliminatoria(partidos);
	}
	
	public static boolean hayCampeon()
	{
		if(campeon.compareToIgnoreCase("NONE") == 0)
			return false;
		
		return true;
	}
	
	public static String getFechaActual()
	{
		switch(fechaActualNum)
		{
			case FASE_GRUPOS_FECHA_1:
				return "Grupos - Fecha 1";
				
			case FASE_GRUPOS_FECHA_2:
				return "Grupos - Fecha 2";
				
			case FASE_GRUPOS_FECHA_3:
				return "Grupos - Fecha 3";

			case FASE_OCTAVOS:
				return "Octavos de Final";
				
			case FASE_CUARTOS:
				return "Cuartos de Final";
				
			case FASE_SEMIS:
				return "Semifinal";
				
			case FASE_FINAL:
				return "Final!!";
				
			case FASE_TERMINO:
				return campeon + " campeon.";
				
			default:
				return "???";
		}
	}
	
	public static ArrayList<String> getOctavos()
	{
		return octavos;
	}
	
	public static ArrayList<String> getCuartos()
	{
		return cuartos;
	}
	
	public static ArrayList<String> getSemifinales()
	{
		return semifinales;
	}
	
	public static ArrayList<String> getFinales()
	{
		return finales;
	}
	
	public static String getCampeon()
	{
		return campeon;
	}
	
	public static EquipoDefault getEquipo(int index) 
	{
		if(index > -1 && index < equipos.size())
			return equipos.get(index);
		
		return null;
	}
	
	public static EquipoDefault getEquipo(String equipo)
	{
		for(EquipoDefault ed : equipos)
		{
			if(ed.getNombre().compareTo(equipo) == 0)
				return ed;
		}
		
		return null;
	}
	
}
