package grandt.base;

import java.util.ArrayList;
import java.util.Collections;

import grandt.base.equipos.EquipoData;

/**
 * <p>Clase que distribuye a los equipos en una tabla (utilizado para la fase de grupos)</p>
 */
public class Tabla {

	private ArrayList<EquipoData> equipos;
	
	public Tabla(ArrayList<EquipoData> equipos)
	{
		this.equipos = equipos;
	}
	
	public void setEquipos(ArrayList<EquipoData> equipos)
	{
		this.equipos = equipos;
	}
	
	public ArrayList<EquipoData> getEquipos()
	{
		return equipos;
	}
	
	/**
	 * <p>Devuelve el primer equipo de la tabla seg�n puntos y, en caso de empate, diferencia de gol.</p>
	 */
	public EquipoData primero()
	{
		Collections.sort(equipos);
		return equipos.get(0);
	}
	
	/**
	 * <p>Devuelve el segundo equipo de la tabla seg�n puntos y, en caso de empate, diferencia de gol.</p>
	 */
	public EquipoData segundo()
	{
		Collections.sort(equipos);
		return equipos.get(1);
	}
	
	@Override
	public String toString()
	{
		return equipos.toString();
	}
}
