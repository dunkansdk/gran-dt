package grandt.base.stats;

import java.util.HashMap;

/**
 * <p>Clase que guarda las estad�sticas (goles, amarillas y asistencias) de todos los jugadores en las fechas
 * simuladas hasta el momento.</p>
 */
public class Stats {

	private static HashMap<String, Integer> goleadores;
	private static HashMap<String, Integer> asistidores;
	private static HashMap<String, Integer> amonestados;

	public static void init()
	{
		goleadores = new HashMap<String, Integer>();
		asistidores = new HashMap<String, Integer>();
		amonestados = new HashMap<String, Integer>();
	}
	
	/**
	 * <p>Edita (agrega si no existe o modifica si ya existe) un goleador en la tabla de goleadores</p>
	 * @param nombre		&emsp;(<b>String</b>) el nombre del goleador a agregar / modificar
	 * @param goles			&emsp;(<b>int</b>) la cantidad de goles que convirti� en la fecha en cuesti�n
	 */
	public static void editGoleador(String nombre, int goles)
	{
		if(goleadores.containsKey(nombre))
		{
			int golesTotales = goleadores.get(nombre) + goles;
			goleadores.remove(nombre);
			goleadores.put(nombre, golesTotales);
		} else {
			goleadores.put(nombre, goles);
		}
	}
	
	/**
	 * <p>Edita (agrega si no existe o modifica si ya existe) un asistidor en la tabla de asistidores</p>
	 * @param nombre		&emsp;(<b>String</b>) el nombre del asistidor a agregar / modificar
	 * @param asis			&emsp;(<b>int</b>) la cantidad de asistencias que ejecut� en la fecha en cuesti�n
	 */
	public static void editAsistidor(String nombre, int asis)
	{
		if(asistidores.containsKey(nombre))
		{
			int asistenciasTotales = asistidores.get(nombre) + asis;
			asistidores.remove(nombre);
			asistidores.put(nombre, asistenciasTotales);
		} else {
			asistidores.put(nombre, asis);
		}
	}
	
	/**
	 * <p>Edita (agrega si no existe o modifica si ya existe) una (o dos) amarilla en la tabla de amarillas</p>
	 * @param nombre		&emsp;(<b>String</b>) el nombre del jugador que recibi� la/s amarilla/s
	 * @param amarillas		&emsp;(<b>int</b>) la cantidad de amarillas que recibi� en la fecha en cuesti�n
	 */
	public static void editAmonestado(String nombre, int amarillas)
	{
		if(amonestados.containsKey(nombre))
		{
			int amarillasTotales = amonestados.get(nombre) + amarillas;
			amonestados.remove(nombre);
			amonestados.put(nombre, amarillasTotales);
		} else {
			amonestados.put(nombre, amarillas);
		}
	}
	
	public static HashMap<String, Integer> getGoleadores()
	{
		return goleadores;
	}
	
	public static HashMap<String, Integer> getAsistidores()
	{
		return asistidores;
	}
	
	public static HashMap<String, Integer> getAmonestados()
	{
		return amonestados;
	}
}
