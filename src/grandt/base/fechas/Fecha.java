package grandt.base.fechas;

import java.util.ArrayList;

import grandt.base.DT;
import grandt.base.Partido;
import grandt.base.Tabla;
import grandt.base.Torneo;
import grandt.base.equipos.EquipoData;
import grandt.base.estados.StateManager;
import grandt.base.exceptions.NonSimulatedMatchException;
import grandt.base.jugador.Arquero;
import grandt.base.jugador.JugadorData;
import grandt.base.stats.Stats;
import grandt.utils.Constants;


/**
 * <p>Clase donde se almacenan los datos de una Fecha (compuesta de varios partidos).</p>
 */
public class Fecha {
	
	protected ArrayList<Partido> partidos;
	
	public Fecha(ArrayList<Partido> partidos) 
    {
		this.partidos = partidos;
	}

	/**
	 * @see grandt.base.Partido Partido
	 */
	public ArrayList<Partido> getPartidos() 
    {
		return partidos;
	}
	
	/**
	 * <p>Simula todos los partidos de la fecha</p>
	 * @see grandt.base.Partido#simular() Partido.simular()
	 */
	public void simular()
	{		
		try {
			// simulamos
			for(Partido p : partidos)
				p.simular();
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			// calculamos
			for(Partido p : partidos)
			{
				// guardamos las estadisticas de los partidos ya simulados en el arraylist "datos"
				ArrayList<JugadorData> datos = p.getStats();
				int golesLocal = 0, golesVisitante = 0, difGolLocal = 0, difGolVisitante = 0;
				
				// guardamos la data de los arqueros por si hay que restar puntos segun goles en contra
				JugadorData arqlocal = null, arqvisitante = null;
				
				// iteramos los datos para ver que equipo gano y la dif de gol
				for(JugadorData jd : datos)
				{
					if(jd.getJugador() instanceof Arquero && jd.getJugador().equals(p.getLocal().getArquero()))
						arqlocal = jd;
					else if(jd.getJugador() instanceof Arquero && jd.getJugador().equals(p.getVisitante().getArquero()))
						arqvisitante = jd;
					
					if(jd.getJugador().getEquipo().compareTo(p.getLocal().getNombre()) == 0)
					{
						if(jd.getGoles() > 0)
						{
							golesLocal += jd.getGoles();
							Stats.editGoleador(jd.getJugador().getNombre(), jd.getGoles());
						}
					} else {
						if(jd.getGoles() > 0)
						{
							golesVisitante += jd.getGoles();
							Stats.editGoleador(jd.getJugador().getNombre(), jd.getGoles());
						}
					}
					
					if(jd.getAsistencias() > 0)
						Stats.editAsistidor(jd.getJugador().getNombre(), jd.getAsistencias());
					
					if(jd.getExpulsion())
						Stats.editAmonestado(jd.getJugador().getNombre(), 2);
					else if(jd.getAmarilla())
						Stats.editAmonestado(jd.getJugador().getNombre(), 1);
					
					if(DT.existeJugador(jd.getJugador()) && !(jd.getJugador() instanceof Arquero))
					{
						StateManager.addMessageJugadores(jd.getJugador().getNombre() + " [" + jd.getPuntaje() + "]:");
						StateManager.addMessageJugadores("\tGoles: " + jd.getGoles() + ", Asistencias: " + 
												jd.getAsistencias() + "\n\tAmonestado: " + String.valueOf(jd.getAmarilla()) + 
												", Expulsado: " + String.valueOf(jd.getExpulsion()));
						DT.sumarPuntos(jd.getPuntaje());
					}
				}
				
				difGolLocal = golesLocal - golesVisitante;
				difGolVisitante = golesVisitante - golesLocal;
				StateManager.addMessageResultados(p.getLocal().getNombre() + " " + golesLocal + " - " + golesVisitante + " " + p.getVisitante().getNombre());
			
				// actualizamos la tabla del torneo
				String grupo = p.getLocal().getGrupo();
				
				Tabla t = Torneo.getTabla(grupo);
				ArrayList<EquipoData> equipos = t.getEquipos();
				
				for(EquipoData eq : equipos)
				{
					if(eq.getEquipo().equals(p.getLocal()))
					{
						eq.aumentarDiferenciaGol(difGolLocal);
						
						if(difGolLocal > 0) // gano
							eq.aumentarPuntos(3);
						else if(difGolLocal == 0) // empato
							eq.aumentarPuntos(1);
						
					} else if(eq.getEquipo().equals(p.getVisitante())) {
						
						eq.aumentarDiferenciaGol(difGolVisitante);
						
						if(difGolVisitante > 0) // gano
							eq.aumentarPuntos(3);
						else if(difGolVisitante == 0) // empato
							eq.aumentarPuntos(1);
						
					}
				}
				
				// vemos si el arquero del DT se comio algun gol
				Arquero arqueroDT = DT.getEquipo().getArquero();

				if(p.getLocal().getArquero().equals(arqueroDT))
				{
					arqlocal.setPuntaje(arqlocal.getPuntaje() - golesVisitante * Constants.PUNTOS_GOL_EN_CONTRA);
					StateManager.addMessageJugadores(arqlocal.getJugador().getNombre() + " [" + arqlocal.getPuntaje() + "]:");
					StateManager.addMessageJugadores("\tGoles en contra: " + golesVisitante);
					DT.sumarPuntos(arqlocal.getPuntaje());
				}
				else if(p.getVisitante().getArquero().equals(arqueroDT))
				{
					arqvisitante.setPuntaje(arqvisitante.getPuntaje() - golesLocal * Constants.PUNTOS_GOL_EN_CONTRA);
					StateManager.addMessageJugadores(arqvisitante.getJugador().getNombre() + " [" + arqvisitante.getPuntaje() + "]:");
					StateManager.addMessageJugadores("\tGoles en contra: " + golesLocal);
					DT.sumarPuntos(arqvisitante.getPuntaje());
				}
			}

		} catch (NonSimulatedMatchException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() 
    {
		return "Fecha [partidos=" + partidos + "]";
	}

}
