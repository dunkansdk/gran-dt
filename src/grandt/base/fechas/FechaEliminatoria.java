package grandt.base.fechas;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import grandt.base.DT;
import grandt.base.Partido;
import grandt.base.equipos.EquipoDefault;
import grandt.base.estados.StateManager;
import grandt.base.exceptions.NonSimulatedMatchException;
import grandt.base.jugador.Arquero;
import grandt.base.jugador.JugadorData;
import grandt.base.stats.Stats;
import grandt.utils.Constants;

/**
 * <p>Clase que engloba las fases eliminatorias del mundial (octavos, cuartos, semis, final)</p>
 */
public class FechaEliminatoria extends Fecha {

	private ArrayList<EquipoDefault> ganadores;
	private boolean simulada;
	
	public FechaEliminatoria(ArrayList<Partido> partidos)
	{
		super(partidos);
		ganadores = new ArrayList<EquipoDefault>();
		simulada = false;
	}
	
	/**
	 * <p>Simula todos los partidos de la fecha. Adem�s, simula penales si el partido est� empatado, y agrega el ganador
	 * de cada cruce al vector "ganadores" de esta clase.</p>
	 * @see grandt.base.Partido#simular() Partido.simular()
	 */
	@Override
	public void simular()
	{
		try {
			// simulamos
			for(Partido p : partidos)
				p.simular();
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			// calculamos
			for(Partido p : partidos)
			{
				// guardamos las estadisticas de los partidos ya simulados en el arraylist "datos"
				ArrayList<JugadorData> datos = p.getStats();
				int golesLocal = 0, golesVisitante = 0, difGolLocal = 0, difGolVisitante = 0;
				
				// guardamos la data de los arqueros por si hay que restar puntos segun goles en contra
				JugadorData arqlocal = null, arqvisitante = null;
				
				// iteramos los datos para ver que equipo gano y la dif de gol
				for(JugadorData jd : datos)
				{
					if(jd.getJugador() instanceof Arquero && jd.getJugador().equals(p.getLocal().getArquero()))
						arqlocal = jd;
					else if(jd.getJugador() instanceof Arquero && jd.getJugador().equals(p.getVisitante().getArquero()))
						arqvisitante = jd;
					
					if(jd.getJugador().getEquipo().compareTo(p.getLocal().getNombre()) == 0)
					{
						if(jd.getGoles() > 0)
						{
							golesLocal += jd.getGoles();
							Stats.editGoleador(jd.getJugador().getNombre(), jd.getGoles());
						}
					} else {
						if(jd.getGoles() > 0)
						{
							golesVisitante += jd.getGoles();
							Stats.editGoleador(jd.getJugador().getNombre(), jd.getGoles());
						}
					}
					
					if(jd.getAsistencias() > 0)
						Stats.editAsistidor(jd.getJugador().getNombre(), jd.getAsistencias());
					
					if(jd.getExpulsion())
						Stats.editAmonestado(jd.getJugador().getNombre(), 2);
					else if(jd.getAmarilla())
						Stats.editAmonestado(jd.getJugador().getNombre(), 1);
					
					if(DT.existeJugador(jd.getJugador()) && !(jd.getJugador() instanceof Arquero))
					{
						StateManager.addMessageJugadores(jd.getJugador().getNombre() + " [" + jd.getPuntaje() + "]:");
						StateManager.addMessageJugadores("\tGoles: " + jd.getGoles() + ", Asistencias: " + 
												jd.getAsistencias() + "\n\tAmonestado: " + String.valueOf(jd.getAmarilla()) + 
												", Expulsado: " + String.valueOf(jd.getExpulsion()));
						DT.sumarPuntos(jd.getPuntaje());
					}
				}
				
				difGolLocal = golesLocal - golesVisitante;
				difGolVisitante = golesVisitante - golesLocal;
				StateManager.addMessageResultados(p.getLocal().getNombre() + " " + golesLocal + " - " + golesVisitante + " " + p.getVisitante().getNombre());
				
				if(difGolLocal > difGolVisitante)
					ganadores.add(p.getLocal());
				else if(difGolLocal < difGolVisitante)
					ganadores.add(p.getVisitante());
				else {
					// SIMULAMOS PENALES
					StateManager.addMessageResultados("[PENALES]\n");
					
					boolean done = false;
					int penalesMetidosLocal = 0, penalesMetidosVisitante = 0, penalesSimulados = 0;
					while(!done)
					{
						// simulamos penal
						StringBuilder sb = new StringBuilder();
						int rnd = ThreadLocalRandom.current().nextInt(0, 100);
						if(rnd > 20)
						{
							sb.append("X - ");
							penalesMetidosLocal++;
						} else {
							sb.append("O - ");
						}
						
						rnd = ThreadLocalRandom.current().nextInt(0, 100);
						if(rnd > 20)
						{
							sb.append("X");
							penalesMetidosVisitante++;
						} else {
							sb.append("O");
						}
						
						StateManager.addMessageResultados(sb.toString());
						
						if(penalesMetidosLocal - penalesMetidosVisitante > 2)
						{
							StateManager.addMessageResultados("Pasa " + p.getLocal().getNombre());
							ganadores.add(p.getLocal());
							done = true;
						} else if (penalesMetidosVisitante - penalesMetidosLocal > 2) {
							StateManager.addMessageResultados("Pasa " + p.getVisitante().getNombre());
							ganadores.add(p.getVisitante());
							done = true;
						} else {
							
							if(penalesMetidosLocal - penalesMetidosVisitante > 1 && penalesSimulados >= 3)
							{
								StateManager.addMessageResultados("Pasa " + p.getLocal().getNombre());
								ganadores.add(p.getLocal());
								done = true;
							} else if (penalesMetidosVisitante - penalesMetidosLocal > 1 && penalesSimulados >= 3) {
								StateManager.addMessageResultados("Pasa " + p.getVisitante().getNombre());
								ganadores.add(p.getVisitante());
								done = true;
							} else {
								
								if(penalesMetidosLocal > penalesMetidosVisitante && penalesSimulados >= 4)
								{
									StateManager.addMessageResultados("Pasa " + p.getLocal().getNombre());
									ganadores.add(p.getLocal());
									done = true;
								} else if (penalesMetidosVisitante > penalesMetidosLocal && penalesSimulados >= 4){
									StateManager.addMessageResultados("Pasa " + p.getVisitante().getNombre());
									ganadores.add(p.getVisitante());
									done = true;
								}
							}
						}

						penalesSimulados++;
					}
					
					StateManager.addMessageResultados("");
				}
				
				// vemos si el arquero del DT se comio algun gol
				Arquero arqueroDT = DT.getEquipo().getArquero();

				if(p.getLocal().getArquero().equals(arqueroDT))
				{
					arqlocal.setPuntaje(arqlocal.getPuntaje() - golesVisitante * Constants.PUNTOS_GOL_EN_CONTRA);
					StateManager.addMessageJugadores(arqlocal.getJugador().getNombre() + " [" + arqlocal.getPuntaje() + "]:");
					StateManager.addMessageJugadores("\tGoles en contra: " + golesVisitante);
					DT.sumarPuntos(arqlocal.getPuntaje());
				}
				else if(p.getVisitante().getArquero().equals(arqueroDT))
				{
					arqvisitante.setPuntaje(arqvisitante.getPuntaje() - golesLocal * Constants.PUNTOS_GOL_EN_CONTRA);
					StateManager.addMessageJugadores(arqvisitante.getJugador().getNombre() + " [" + arqvisitante.getPuntaje() + "]:");
					StateManager.addMessageJugadores("\tGoles en contra: " + golesLocal);
					DT.sumarPuntos(arqvisitante.getPuntaje());
				}
			}

		} catch (NonSimulatedMatchException e) {
			e.printStackTrace();
		}
		
		simulada = true;
	}
	
	public ArrayList<EquipoDefault> getGanadores() throws NonSimulatedMatchException
	{
		if(!simulada)
			throw new NonSimulatedMatchException();
		
		return ganadores;
	}
}
