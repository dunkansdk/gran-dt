package grandt.base;

import grandt.utils.Constants;

/**
 * <p>Clase que guarda los datos de una formacion (por el momento, hay 4 formaciones posibles),</p>
 */
public class Formacion {
	
	// FORMACIONES : 4 - 3 - 3 (4def, 3med, 3del)
	//				 4 - 4 - 2
	//  			 3 - 4 - 3
	// 				 5 - 3 - 2
	
	private int defensores;
	private int volantes;
	private int delanteros;
	
	public Formacion(int defensores, int volantes, int delanteros) {
		this.defensores = defensores;
		this.volantes = volantes;
		this.delanteros = delanteros;
	}
	
	/**
	 * <p>Devuelve la cantidad de jugadores que hay en una posicion determinada</p>
	 * @param pos		&emsp;(<b>int</b>) la posicion a buscar 
	 * @return	(<b>int</b>) la cantidad de jugadores en la posicion buscada
	 * @see grandt.utils.Constants Constants (para constantes de posicion)
	 */
	public int getJugadoresPos(int pos) {
		switch (pos)
		{
			case Constants.POS_DEFENSOR: return defensores;
			case Constants.POS_VOLANTE: return volantes;
			case Constants.POS_DELANTERO: return delanteros;
			default: return 0;
		}
	}
	
	@Override
	public String toString()
	{
		return defensores + "-" + volantes + "-" + delanteros;
	}
}
