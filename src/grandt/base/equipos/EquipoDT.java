package grandt.base.equipos;

import grandt.base.jugador.Arquero;
import grandt.base.jugador.Jugador;

/**
 * <p>Equipo del usuario.</p>
 * <p>Las funcionalidades de manejo de este equipo est�n, principalmente, en la clase {@link grandt.base.DT DT}.</p>
 * @see grandt.base.equipos.Equipo
 */
public class EquipoDT extends Equipo {

	public EquipoDT(String nombre) {
		super(nombre);
	}
	
	public void agregarJugador(Jugador jugador) {
		jugadores.add(jugador);
	}
	
	public void eliminarJugador(Jugador jugador) {
		jugadores.remove(jugador);
	}

	public boolean full() {
		return (jugadores.size() == JUGADORES_TOTALES);
	}
	
	public void reset()
	{
		jugadores.clear();
	}
	
	public void setNombre(String s)
	{
		nombre = s;
	}
	
	@Override
	public Arquero getArquero()
	{
		for(Jugador j : jugadores)
			if(j instanceof Arquero) return (Arquero) j;
		
		return null;
	}
}
