package grandt.base.equipos;

import java.util.ArrayList;

import grandt.base.jugador.Arquero;
import grandt.base.jugador.Campo;
import grandt.base.jugador.Jugador;

/**
 * <p>Clase que almacena los datos de un Equipo</p>
 * <p>Es abstracta porque un Equipo ser� EquipoDT (el equipo del usuario) o bien EquipoDefault (los equipos precargados) </p>
 */
public abstract class Equipo {
	
	protected static final int JUGADORES_TOTALES = 11;
	
	protected String nombre;
	protected ArrayList<Jugador> jugadores;
	
	public abstract Arquero getArquero();
	
	public Equipo(String nombre) 
    {
		this.nombre = nombre;
		jugadores = new ArrayList<Jugador>();
	}
	
	public String getNombre() 
    {
		return nombre;
	}

	public ArrayList<Jugador> getJugadores() 
    {
		return jugadores;
	}
	
	public Jugador getJugador(String nombre)
	{
		for(Jugador j : jugadores)
			if(j.getNombre().compareTo(nombre) == 0)
				return j;
		
		return null;
	}
	
	public Jugador getJugador(int index)
	{
		return jugadores.get(index);
	}
	
	public boolean hayArquero() {
		for(Jugador j : jugadores) 
			if(j instanceof Arquero) return true;
		return false;
	}
	
	/**
	 * <p>Devuelve la cantidad de jugadores que hay en una posici�n particular</p>
	 * @param pos		&emsp;(<b>int</b>) la posici�n (ver {@link grandt.utils.Constants Constants} para enteros de posicion)
	 * @return			&emsp;(<b>int</b>) la cantidad de jugadores en dicha posici�n
	 */
	public int getJugadoresPos(int pos) {
		int counter = 0;
		
		for(Jugador j : jugadores) {
			if(j instanceof Campo) 
			{
				Campo jc = (Campo) j;
				if(jc.getPosicion() == pos) counter++;
			}
		}
		
		return counter;
	}

	@Override
	public String toString() {
		return "Equipo [nombre=" + nombre + ", jugadores=" + jugadores + "]";
	}

}
