package grandt.base.equipos;

import java.util.ArrayList;

import grandt.base.exceptions.IncompleteTeamException;
import grandt.base.jugador.Arquero;
import grandt.base.jugador.Jugador;

/**
 * <p>Clase para los equipos precargados (desde el JSON)</p>
 * @see grandt.base.equipos.Equipo Equipo
 */
public class EquipoDefault extends Equipo {

	private String grupo;
	
	/**
	 * <p>Constructor de EquipoDefault</p>
	 * @param nombre					&emsp;(<b>String</b>) el nombre del equipo
	 * @param grupo						&emsp;(<b>String</b>) el grupo en el que juega (la fase de grupos)
	 * @param jugadores					&emsp;({@link java.util.ArrayList ArrayList[Jugador]}) el vector de jugadores
	 * @throws IncompleteTeamException	&emsp;({@link Exception}) si recibe un vector con menos de 11 jugadores
	 * @see grandt.base.jugador.Jugador Jugador
	 */
	public EquipoDefault(String nombre, String grupo, ArrayList<Jugador> jugadores) throws IncompleteTeamException {
		super(nombre);
		this.grupo = grupo;
		if(jugadores.size() < JUGADORES_TOTALES) {
			throw new IncompleteTeamException();
		} else {
			this.jugadores = jugadores;
		}
	}
	
	public String getGrupo() 
    {
		return grupo;
	}
	
	/**
	 * <p>Calcula el rating total (suma del rating de todos los jugadores) del equipo y lo retorna</p>
	 * @return		&emsp;(<b>int</b>) rating total
	 */
	public int getRating()
	{
		int ret = 0;
		
		for(Jugador jugador : jugadores)
			ret += (int) jugador.getCalidad() * 100;
		
		return ret;
	}
	
	/**
	 * <p>Retorna la suma de la chance de amarilla de todos los jugadores del equipo; este dato se usa posteriormente
	 * para estimar la cantidad de faltas que infringe el equipo en un partido</p>
	 * @return		&emsp;(<b>int</b>) faltas totales
	 */
	public int getFaltas()
	{
		int ret = 0;
		
		for(Jugador jugador : jugadores)
			ret += (int) jugador.getChanceAmarilla();
		
		return ret;
	}
	
	@Override
	public Arquero getArquero()
	{
		// al estar en EquipoDefault tenemos la certeza (por como se cargan los JSON) de que el arquero es el jugador en el �ndice 0 del arreglo
		return (Arquero) jugadores.get(0);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o != null && o instanceof EquipoDefault)
		{
			if(((EquipoDefault) o).getNombre().compareTo(this.nombre) == 0)
				return true;
		}
		
		return false;
	}
}
