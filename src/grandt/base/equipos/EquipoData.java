package grandt.base.equipos;

/**
 * <p>A diferencia de Equipo, esta clase guarda todos los datos de un equipo para un torneo
 * determinado.</p>
 * @see grandt.base.equipos.EquipoDefault EquipoDefault
 * @see java.lang.Comparable Comparable
 */
public class EquipoData implements Comparable<EquipoData> {
	
	private EquipoDefault equipo;
	private int puntos;
	private int diferenciaGol;
	
	public EquipoData(EquipoDefault equipo) {
		this.equipo = equipo;
		this.puntos = 0;
		this.diferenciaGol = 0;
	}
	
	/**
	 * <p>Aumenta los puntos en del equipo</p>
	 * @param puntos	&emsp;(3 gano, 1 emapto)
	 * @see grandt.base.Tabla
	 */
	public void aumentarPuntos(int puntos) {
		this.puntos += puntos;
	}
	
	/**
	 * <p>Aumenta la diferencia de gol en el equipo con respecto a los rivales</p>
	 * @param diferenciaGol
	 */
	public void aumentarDiferenciaGol(int diferenciaGol) {
		this.diferenciaGol += diferenciaGol;
	}

	public EquipoDefault getEquipo() {
		return equipo;
	}

	public int getPuntos() {
		return puntos;
	}

	public int getDiferenciaGol() {
		return diferenciaGol;
	}
	
	@Override
	public String toString()
	{
		return equipo.getNombre() + ", PTS: " + puntos + ", DIF:" + diferenciaGol;
	}

	@Override
	public int compareTo(EquipoData o) {
		if(o != null)
		{
			if(this.puntos > o.getPuntos())
				return -1;
			else if(this.puntos == o.getPuntos())
			{
				if(this.diferenciaGol > o.getDiferenciaGol())
					return -1;
				else
					return 1;
			}	
		}
		
		return 1;
	}

}
