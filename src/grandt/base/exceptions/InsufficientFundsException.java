package grandt.base.exceptions;

/**
 * <p>Excepci�n de fondos insuficientes (no alcanza para comprar a X jugador)</p>
 */
public class InsufficientFundsException extends Exception {
    
	private static final long serialVersionUID = 1L;

	public InsufficientFundsException() 
    {
        super("Fondos insuficientes.");
    }
    
}
