package grandt.base.exceptions;

/**
 * <p>Excepci�n de equipo incompleto (no tiene 11 jugadores)</p>
 */
public class IncompleteTeamException extends Exception {

	private static final long serialVersionUID = 1L;

	public IncompleteTeamException() 
    {
        super("El arreglo de jugadores que se quiere ingresar al equipo no esta completo, revisar JSON");
    }

}
