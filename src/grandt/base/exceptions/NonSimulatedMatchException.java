package grandt.base.exceptions;

/**
 * <p>Excepci�n de partido no simulado (cuando se trata de acceder a la estad�sticas de un partido que a�n no se simul�).</p>
 */
public class NonSimulatedMatchException extends Exception {

	private static final long serialVersionUID = 1L;

	public NonSimulatedMatchException() {
		super("El partido no fue simulado aun, imposible acceder a las estadisticas de los jugadores");
	}
}
