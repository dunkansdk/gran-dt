package grandt.base.exceptions;

/**
 * <p>Excepci�n de formaci�n inv�lida (cuando se tratan de agregar m�s jugadores en una posici�n de los que admite
 * el tipo de formaci�n)</p>
 */
public class InvalidFormationException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public InvalidFormationException(String p) {
		super("Error en formacion: " + p);
	}

}
