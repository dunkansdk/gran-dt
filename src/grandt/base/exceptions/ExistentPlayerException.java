package grandt.base.exceptions;

/**
 * <p>Excepci�n de jugador existente (ya est� en el equipo)</p>
 */
public class ExistentPlayerException extends Exception {

	private static final long serialVersionUID = 1L;

	public ExistentPlayerException() 
    {
        super("Jugador ya existente.");
    }

}
