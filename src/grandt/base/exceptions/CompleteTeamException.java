package grandt.base.exceptions;

/**
 * <p>Excepci�n de equipo completo (ya tiene 11 jugadores)</p>
 */
public class CompleteTeamException extends Exception {

	private static final long serialVersionUID = 1L;

	public CompleteTeamException() 
    {
        super("El equipo esta completo");
    }
	
}
