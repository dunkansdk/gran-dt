TP Final Programación 3
=======================

## ![alt text](http://www.rochdale.gov.uk/icons/icon4.png "Integrantes") Tabla de Contenidos
> • [Integrantes](#integrantes) <br>
• [Diario de Trabajo](#diario-de-trabajo) <br>
• [Matriz de Soluciones](#matriz-de-soluciones) <br>
• [Diagrama UML](#diagrama-uml) <br>
• [Sistema de Situaciones](#sistema-situaciones) <br>
• [Manual de Usuario](#manual) <br>
• [Fuentes de Información](#fuentes) <br>

## ![alt text](http://www.rochdale.gov.uk/icons/icon23.png "Integrantes") Integrantes: <a name="integrantes"></a>
> #### Borelli, Juan Ignacio
#### D'Urso, Emanuel
#### Liste, Manuel

------------
## ![alt text](http://www.rochdale.gov.uk/icons/icon6.png "Diario de Trabajo") Diaro de Trabajo <a name="diario-de-trabajo"></a>

> **Semana 1 (14/5 - 21/5):**
> - Creada arquitectura básica de clases.
> - Creadas clases para excepciones y carga de archivos.
> - Agregados archivos JSON básicos.
> - Agregada carga de archivos JSON vía Parser y Loaders.
> - Agregada base y funcionalidad de interfaz de creació de equipos.
> - Agregada base de carga de fechas y JSONs correspondientes.
> - Reorganizado el código de los JFrames; agregada nueva jerarquí de herencias (Estados).
> - Se agregaron JSONs para nuevos equipos.

> **Semana 2 (22/5 - 29/5):**
> - Comentado y reorganizado el código.
> - Cambios en nombres de variables para facilitar legibilidad.
> - Rediseño del diagrama UML y del esquema de clases.
> - Agregamos display (Canvas) de cancha.
> - Avances en la funcionalidad de creación de equipo.
> - Agregada clase Formación y Excepción correspondiente.
> - Finalizado el display de la cancha.
> - Creada base de simulaciones de partidos (sistema de situaciones de gol).
> - Creada base de simulación de faltas (sistema de chance acumulada de amarilla).

> **Semana 3 (30/5 - 6/6):**
> - Refinado y modularizado el sistema de situaciones.
> - Agregados threads para situaciones más realistas.
> - Comentado todo el código y re-generado el javadoc.

> **Semana 4 (7/6 - 14/6):**
> - Agregada base de sistema de tablas de puntos por grupo.
> - Refinado sistema de tablas de grupo.
> - Agregadas tablas de goleadores, asistidores y amarillas.
> - Funcionalidad avanzada del "estado" StateJugando.
> - Agregada funcionalidad para fases eliminatorias (octavos, cuartos, semifinales, final).
> - Agregadas definiciones por penales (random) si un partido de fase eliminatoria es empate.
> - Agregado sistema de puntaje de jugadores; comienzan (cada fecha) con un puntaje default y sumnar o restan según corresponda.

> **Semana 5 (15/6 - 21/6):**
> - Agregada ventana que muestra los cruces de cada fase eliminatoria.
> - Retocados valores constantes para mayor realismo en las simulaciones.
> - Agregados detalles estéticos en las interfaces.

------------
## ![alt text](http://www.rochdale.gov.uk/icons/icon2.png "Matriz de Soluciones") Matriz de Soluciones <a name="matriz-de-soluciones"></a>

| Problema | Causa | Solución |
|:----------|:----------|:----------|
| Error en la carga de archivos JSON | Rutas erróneas y datos faltantes | Corregimos los archivos según correspondía |
| Errores en los listeners de los widgets de la interfaz de creación de equipos | Estaban mal creados y no se linkeaban al widget | Agregamos los ".add()" correspondientes |
| Error al intentar crear "estados" con JFrames independientes | Errores conceptuales varios que solucionamos investigando | Corregimos y organizamos agregando una interfaz y las clases correspondientes |
| Error al intentar dibujar el display de la cancha en un JCanvas | Nuevamente, era un error por desconocimiento | Solucionamos investigando |
| Error al intentar simular ciertos partidos | Los partidos aún no estaban cargados (faltan JSONs), por suerte, tenemos Excepciones propias que nos ayudan =) | Postergamos la simulación de dichos partidos hasta tener los JSON disponibles |
| Error al intentar cargar el HashMap de tablas | Usábamos el mismo vector para todas las tablas y se pisaba en cada iteración | Creamos nuevos vectores según correspondía |
| Error al intentar crear sub-ventanas en StateJugando | Había una llamada a ventana.removeAll() que borraba los componentes | Borramos la llamada |
| Error al intentar obtener el primer y segundo equipo de cada grupo | Suponíamos que el arreglo ya estaba ordenado, perosólo se ordenaba si se había abierto la interfaz de tablas | Hicimos Collections.sort() por más que probablemente ya esté ordenado |
| Error al simular definiciones por penales | Si bien no eran errores de programación, los penales no tenían resultados "realistas" (podía terminar una serie en solo un penal) | Cambiamos el código según correspondía |

------------
## ![alt text](http://www.rochdale.gov.uk/icons/icon3.png "Diagrama UML") Diagrama UML <a name="diagrama-uml"></a>
<details><summary>Click para expandir (VERSION BASE)</summary>
<img alt="Diagrama UML" src="https://i.imgur.com/1ZxD36t.png">
</details>
<details><summary>Click para expandir (VERSION FINAL)</summary>
<img alt="Diagrama UML" src="https://i.imgur.com/irsvvTO.png">
</details>

------------
## ![alt text](http://www.rochdale.gov.uk/icons/icon8.png "Sistema de Situaciones") Sistema de Situaciones <a name="sistema-situaciones"></a>
<details><summary>Click para expandir</summary>
<img alt="Sistema de Situaciones" src="https://i.imgur.com/fkED0Vm.png">
</details>

------------
## ![alt text](http://www.rochdale.gov.uk/icons/icon21.png "Manual de Usuario") Manual de Usuario <a name="manual"></a>
<details><summary>Click para expandir</summary>
<img alt="Manual de Usuario 1" src="https://i.imgur.com/fEul4EC.png">
<img alt="Manual de Usuario 2" src="https://i.imgur.com/ohsQije.png">
</details>

------------
## ![alt text](http://www.rochdale.gov.uk/icons/icon1.png "Fuentes de Información") Fuentes de Información <a name="fuentes"></a>
> Para la programación:
<br>https://stackoverflow.com/
<br>https://docs.oracle.com/javase/8/docs/api/


> Para la carga de estadísticas:
<br>https://es.whoscored.com
<br>https://www.transfermarkt.es/
<br>https://www.json.org/